﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json.Converters;
using System.Text;
using InboundData.Models;
using System.Linq;
using DogModeApp.Clients.Core.Services.Interfaces;
using DogModeApp.Clients.Core.Services;

namespace Tests
{
    [TestClass]
    public class BasicApiIntegration
    {
        
         
        private IFeedService FeedService { get; set; } = null;

        [TestInitialize]
        public void TestInit()
        {
             
            // map implementation with feed service
            FeedService = new FeedService();
        }
 

        [TestMethod]
        public async Task TestGetFeedsByBeaconAsync()
        {
          
            var filter = new FilterOptionModel()
            {
                BeaconId= "B9407F30-F5F8-466E-AFF9-25556B57FE6D",
                Mayor = 19805,
                Menor= 26121,
            };
            
            var feeds = await FeedService.GetFeedsByBeacon(filter);

            Assert.IsTrue(feeds != null && feeds.Any());
        }

        /// <summary>
        /// test purpose load feed details and ge feed by id From Api
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task TestFeedDetailsAsync()
        {
            var feed = await FeedService.GetById(1);
            
            Assert.IsNotNull(feed);
        }

        [TestMethod]
        public async Task TestGetAdsViewedByUser()
        { 
            var feeds = await FeedService.GetViewedFeedsByUser("10154508418477826");

            Assert.IsTrue(feeds != null && feeds.Any());
        }


        [TestMethod]
        public async Task GetRandomActiveFeed()
        {
            var feed = await FeedService.GetOneActive("10154508418477826", "B9407F30-F5F8-466E-AFF9-25556B57FE6D", 19805, 26121);

            Assert.IsTrue(feed != null && feed.Id>0);
        }

    }
}
