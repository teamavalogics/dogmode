using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Preferences;
using DogModeApp.Droid.Shared.Constants;

namespace DogModeApp.Droid.Receivers
{
    [BroadcastReceiver(Enabled = true, Exported = false)]
    [IntentFilter(new[] { IntentFilterConstants.NotificationDismiss })]
    public class NotificationDismissReceiver : BroadcastReceiver
    {
        public override void OnReceive(Context context, Intent intent)
        {
            int adId = intent.GetIntExtra("com.DogModeApp.AdId", -1);
            long adExpirationTicks = intent.GetLongExtra("com.DogModeApp.AdExpirationTicks", 0);
            if (adId >= 0)
            {
                ISharedPreferences prefs = PreferenceManager.GetDefaultSharedPreferences(context);
                ISharedPreferencesEditor editor = prefs.Edit();
                editor.PutLong(adId.ToString(), adExpirationTicks);
                editor.PutBoolean(SharedPreferencesConstants.NotificationDisplayed, false);
                editor.Apply();
            }
        }
    }
}