﻿using Android.App;
using Android.Content.PM;
using Android.OS;
using DogModeApp.Clients.Core;
using DogModeApp.Clients.Core.Providers.Interfaces;
using Microsoft.WindowsAzure.MobileServices;
using System;
using System.Threading.Tasks;
using AppSettings = DogModeApp.Clients.Core.Helpers.Settings;
using DogModeApp.Clients.Core.Services;

using FFImageLoading.Forms.Droid;

namespace DogModeApp.Droid
{
    [Activity(
        Label = "@string/app_name",
        Theme = "@style/MainTheme",
        Icon = "@drawable/ic_launcher",
        //ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation
        ScreenOrientation = ScreenOrientation.Portrait)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity , IAuthenticate
    {
        public async Task<bool> AuthenticateAsync(MobileServiceAuthenticationProvider? type = default(MobileServiceAuthenticationProvider?))
        {
            bool success = false;
            try
            { 
                // The authentication provider could also be Facebook, Twitter, or Microsoft
                user = await AzureClientServiceManager.DefaultManager.CurrentClient.LoginAsync(this, type ?? MobileServiceAuthenticationProvider.Facebook);

                if (user != null)
                {
                    AppSettings.AccessToken = user.MobileServiceAuthenticationToken;
                   

                    //CreateAndShowDialog(string.Format("You are now logged in - {0}", user.UserId), "Logged in!");
                }

                success = true;
            }
            catch (Exception ex)
            {
                CreateAndShowDialog(ex.Message, "Authentication failed");
            }
            return success;
        }

        protected override void OnCreate(Bundle bundle)
        {

            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(bundle);

            global::Xamarin.Forms.Forms.Init(this, bundle);

            CachedImageRenderer.Init();
            
            CurrentPlatform.Init();
            
            App.Init((IAuthenticate)this);

            // get id from advertising created on notification specs
            int appointmentId = Intent.GetIntExtra("com.DogModeApp.ApId", 0);

             
            
            //load  with normal process if id is 0 ,but if is diferent we redirect to details page
            LoadApplication(appointmentId==0 ? new App(): new App(appointmentId) );
        }
        void CreateAndShowDialog(string message, string title)
        {
            var builder = new AlertDialog.Builder(this);
            builder.SetMessage(message);
            builder.SetTitle(title);
            builder.SetNeutralButton("OK", (sender, args) => {
            });
            builder.Create().Show();
        }
        #region Properties
        MobileServiceUser user;

        #endregion
    }
}