
using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using Android.Content.PM;
using Android.Support.V7.App;
using Android.Graphics;
 

namespace DogModeApp.Droid.Activities
{

    [Activity(
        Label = "BeaconApp",
        Icon = "@drawable/ic_launcher",
        Theme = "@style/Theme.Splash",
        MainLauncher = true,
        NoHistory = true,
        ScreenOrientation = ScreenOrientation.Portrait)]
    public class SplashActivity : AppCompatActivity
    {

        #region 
        private TextView splashText;

        private Typeface tf { get; set; }
         
        #endregion
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);




            SetContentView(Resource.Layout.SplashView);

            splashText = (TextView)FindViewById(Resource.Id.SplashText);

            tf = Typeface.CreateFromAsset(Assets, "Sketch.ttf");
            splashText.Typeface = tf;


            InvokeMainActivityAsync();
            
        }

        private void InvokeMainActivity()
        {
            var mainActivityIntent = new Intent(this, typeof(MainActivity));
            StartActivity(mainActivityIntent);

            
        }

        private void InvokeMainActivityAsync()
        {
            
            System.Threading.AutoResetEvent autoEvent = new System.Threading.AutoResetEvent(false);
            System.Threading.Timer stateTimer = new System.Threading.Timer((ob) =>
            {
                this.RunOnUiThread(InvokeMainActivity);
            }, autoEvent, 300, 0);
        }
    }
}