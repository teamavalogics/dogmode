﻿using InboudData.Clients.Core.Controls;
using System;
using System.Globalization;
using Xamarin.Forms;
using static InboudData.Clients.Core.Controls.CustomPin;

namespace InboudData.Clients.Core.Converters
{
    class StationTypeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var customPin = value as CustomPin;

            if(customPin != null)
            {
                return customPin.Type;
            }

            return AnnotationType.Normal;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
