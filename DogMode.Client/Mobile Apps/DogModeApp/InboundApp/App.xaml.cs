﻿using DogModeApp.Clients.Core.Providers.Interfaces;
using DogModeApp.Clients.Core.Views;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace DogModeApp.Clients.Core
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

           
           MainPage =new Drawer();
 
        }
        public App(int appointmentId)
        {
            InitializeComponent();
            MainPage = new Drawer(appointmentId);
        }


        public static void Init(IAuthenticate authenticator)
        {
            Authenticator = authenticator;
        }

        public async Task InitializeNavigationAsync()
        {
            await MainPage.Navigation.PushAsync(new LoginPage());
        }

      

        public static IAuthenticate Authenticator { get; private set; }

    }
}
