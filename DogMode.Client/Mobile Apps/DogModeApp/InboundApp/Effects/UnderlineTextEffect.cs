﻿using Xamarin.Forms;

namespace DogModeApp.Clients.Core.Effects
{
    public class UnderlineTextEffect : RoutingEffect
    {
        public UnderlineTextEffect() : base("DogModeApp.UnderlineTextEffect")
        {
        }
    }
}
