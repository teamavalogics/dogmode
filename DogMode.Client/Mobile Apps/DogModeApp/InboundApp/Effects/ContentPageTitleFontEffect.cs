﻿using Xamarin.Forms;

namespace DogModeApp.Clients.Core.Effects
{
    public class ContentPageTitleFontEffect : RoutingEffect
    {
        public ContentPageTitleFontEffect() : base("DogModeApp.ContentPageTitleFontEffect")
        {
        }
    }
}
