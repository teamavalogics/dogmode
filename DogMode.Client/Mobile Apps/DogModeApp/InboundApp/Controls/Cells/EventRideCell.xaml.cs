﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DogModeApp.Clients.Core.Controls.Cells
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EventRideCell : ViewCell
    {
        public EventRideCell()
        {
            InitializeComponent();
        }
    }
}
