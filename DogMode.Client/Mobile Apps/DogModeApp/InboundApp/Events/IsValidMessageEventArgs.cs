﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DogModeApp.Clients.Core.Events
{
    public class IsValidMessageEventArgs : EventArgs
    {
        public IsValidMessageEventArgs(bool isValid)
        {
            IsValid = isValid;
        }

        public bool IsValid { get; }
    }
}
