﻿namespace DogModeApp.Clients.Core.Validations
{
    public interface IValidity
    {
        bool IsValid { get; set; }
    }
}
