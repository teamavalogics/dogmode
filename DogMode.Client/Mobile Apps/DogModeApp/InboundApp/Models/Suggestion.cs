﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DogModeApp.Clients.Core.Models
{
    public class Suggestion
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string ImagePath { get; set; }

        public int Distance { get; set; }

        public float Latitude { get; set; }

        public float Longitude { get; set; }
    }
}
