﻿using DogModeApp.Clients.Core.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DogModeApp.Clients.Core.Models
{
    public class WeatherInfo : IWeatherResponse
    {
        public float Temp { get; set; }
        public TempUnit TempUnit { get; set; }
        public string LocationName { get; set; }
    }
}
