﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DogModeApp.Clients.Core.Models
{
    public class Event
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string ImagePath { get; set; }

        public DateTime StartTime { get; set; }

        public string ExternalId { get; set; }

        //public Venue Venue { get; set; }
    }
}
