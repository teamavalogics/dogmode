﻿namespace DogModeApp.Clients.Core.Models
{
    public enum TempUnit
    {
        Fahrenheit,
        Celsius,
        Kelvin
    }
}