﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InboundApp.Clients.Core.Models
{
    public class UserProfile
    {
        public string Id { get; set; }

        public int UserId { get; set; }

       // public User User { get; set; }

        public int? Gender { get; set; }

        public DateTime BirthDate { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        //public Payment Payment { get; set; }

        //public int PaymentId { get; set; }

        public string Email { get; set; }

        public string Skype { get; set; }

        public string FaceProfileId { get; set; }

        public string VoiceProfileId { get; set; }

        public string PhotoUrl { get; set; }
    }
}
