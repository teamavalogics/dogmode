﻿using Microsoft.WindowsAzure.MobileServices;
using System.Threading.Tasks;

namespace DogModeApp.Clients.Core.Providers.Interfaces
{
    public interface IAuthenticate
    {
        Task<bool> AuthenticateAsync(MobileServiceAuthenticationProvider? type=null);
         
    }
}
