﻿using InboundApp.Clients.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InboundApp.Clients.Core.Services
{
    public interface ILocationProvider
    {
        Task<ILocationResponse> GetPositionAsync();
    }
}
