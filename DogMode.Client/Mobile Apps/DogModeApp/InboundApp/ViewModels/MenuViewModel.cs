﻿using DogModeApp.Clients.Core.Services;
using DogModeApp.Clients.Core.Services.Interfaces;
using DogModeApp.Clients.Core.Models;
using DogModeApp.Clients.Core.Models.Enums;
using DogModeApp.Clients.Core.ViewModels.Base;
using DogModeApp.Clients.Core.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using MenuItem = DogModeApp.Clients.Core.Models.MenuItem;
using DogMode.Models;

namespace DogModeApp.Clients.Core.ViewModels
{
    public class MenuViewModel : BaseViewModel
    {
        #region properties
        private readonly IProfileService _profileService;
        private readonly IAuthenticationService _authenticationService;

        public ICommand ItemSelectedCommand => new Command<MenuItem>(OnSelectItem);

        public ICommand LogoutCommand => new Command(OnLogout);

        ObservableCollection<MenuItem> menuItems = new ObservableCollection<MenuItem>();

        Action<DogModeApp.Clients.Core.Models.MenuItem> NavigateAction { get; set; } = null;

        #endregion properties

        public ObservableCollection<MenuItem> MenuItems
        {
            get
            {
                return menuItems;
            }
            set
            {
                menuItems = value;
                RaisePropertyChanged(() => MenuItems);
            }
        }

        UserProfile profile;

        public UserProfile Profile
        {
            get
            {
                return profile;
            }

            set
            {
                profile = value;
                RaisePropertyChanged(() => Profile);
                RaisePropertyChanged(() => ProfileFullName);
            }
        }

        public string ProfileFullName
        {
            get
            {
                if (Profile == null)
                {
                    return "-";
                }
                else
                {
                    return Profile.FirstName + " " + Profile.LastName;
                }
            }
        }

        public MenuViewModel(Page page,Action <DogModeApp.Clients.Core.Models.MenuItem> navigateAction)
        {
            this.CurrentPage = page;
            InitMenuItems();

            // TODO implement DI
            _authenticationService = new AuthenticationService();

            this.NavigateAction = navigateAction;
            MessagingCenter.Subscribe<UserProfile>(this, MessengerKeys.ProfileUpdated, OnProfileUpdated);
        }

        //public MenuViewModel(IProfileService profileService, IAuthenticationService authenticationService)
        //{
        //    _profileService = profileService;
        //    _authenticationService = authenticationService;

        //    InitMenuItems();
        //    //MessagingCenter.Subscribe<Booking>(this, MessengerKeys.BookingRequested, OnBookingRequested);
        //    //MessagingCenter.Subscribe<Booking>(this, MessengerKeys.BookingFinished, OnBookingFinished);
        //    //MessagingCenter.Subscribe<UserProfile>(this, MessengerKeys.ProfileUpdated, OnProfileUpdated);
        //}

        //public override async Task InitializeAsync(object navigationData)
        //{
        //    try
        //    {
        //        Profile = await _profileService.GetFacebookProfileAsync();
        //    }
        //    catch (Exception ex) when (ex is WebException || ex is HttpRequestException)
        //    {
        //        await DialogService.ShowAlertAsync("Communication error", "Error", "Ok");
        //    }
        //    catch (Exception ex)
        //    {
        //        Debug.WriteLine($"Error fetching profile with exception: {ex}");
        //    }
        //}

        private void InitMenuItems()
        {
            if (Device.OS == TargetPlatform.Windows)
            {
                MenuItems.Add(new MenuItem
                {
                    Title = "Inicio",
                    MenuItemType = MenuItemType.Home,
                    ViewModelType = typeof(HomePage),
                    IsEnabled = true,
                    
                });

                //MenuItems.Add(new MenuItem
                //{
                //    Title = "New Ride",
                //    MenuItemType = MenuItemType.NewRide,
                //    ViewModelType = typeof(CustomRideViewModel),
                //    IsEnabled = true
                //});
            }

            if (Device.OS == TargetPlatform.Android)
            {
                //MenuItems.Add(new MenuItem
                //{
                //    Title = "New Ride",
                //    MenuItemType = MenuItemType.NewRide,
                //    ViewModelType = typeof(CustomRideViewModel),
                //    IsEnabled = true
                //});
            }

            //MenuItems.Add(new MenuItem
            //{
            //    Title = "My Rides",
            //    MenuItemType = MenuItemType.MyRides,
            //    ViewModelType = Device.Idiom == TargetIdiom.Desktop ? typeof(UwpMyRidesViewModel) : typeof(MyRidesViewModel),
            //    IsEnabled = true
            //});

            //MenuItems.Add(new MenuItem
            //{
            //    Title = "Inicio",
            //   // MenuItemType = MenuItemType.Profile,
            //    ViewModelType = typeof(HomePage),
            //    IsEnabled = true
            //});



            MenuItems.Add(new MenuItem
            {
                Title = "Mi Perfil",
                // MenuItemType = MenuItemType.Profile,
                ViewModelType = typeof(ProfilePage),
                IsEnabled = true
            });

            //MenuItems.Add(new MenuItem
            //{
            //    Title = "Configuracion",
            //    //MenuItemType = MenuItemType.Profile,
            //    ViewModelType = typeof(BeaconSettings),
            //    IsEnabled = true
            //});


        }

        private async void OnSelectItem(MenuItem item)
        {
            if (item.IsEnabled)
            {
                object parameter = null;

                if (item.MenuItemType == MenuItemType.UpcomingRide)
                { ;
                }

                //await CurrentPage.Navigation.PushAsync((Page)Activator.CreateInstance(item.ViewModelType));
                if (NavigateAction != null)
                    NavigateAction(item);
            }
        }

        private async void OnLogout()
        {
            await _authenticationService.LogoutAsync();
            var item = new DogModeApp.Clients.Core.Models.MenuItem()
            {
                Title ="LoginView",
                ViewModelType  = typeof(LoginPage)
            };
             NavigateAction(item);
        }

     

        private void SetMenuItemStatus(MenuItemType type, bool enabled)
        {
            MenuItem rideItem = MenuItems.FirstOrDefault(m => m.MenuItemType == type);

            if (rideItem != null)
            {
                rideItem.IsEnabled = enabled;
            }
        }

        private async void OnProfileUpdated(UserProfile profile)
        {
            Profile = null;

            if (Device.OS == TargetPlatform.Windows)
            {
                await Task.Delay(2000); // Give UWP enough time (for Photo reload)
            }

            Profile = profile;
            
        }

         
    }
}
