﻿using DogModeApp.Clients.Core.Services;
using DogModeApp.Clients.Core.Services.Interfaces;
using DogModeApp.Clients.Core.Helpers;
using DogModeApp.Clients.Core.Models;
using DogModeApp.Clients.Core.ViewModels.Base;
using DogModeApp.Clients.Core.Views;
 
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using DogMode.Models;

namespace DogModeApp.Clients.Core.ViewModels
{
    public class ProfileViewModel : BaseViewModel
    {
        private UserProfile _profile;

        private readonly IProfileService _profileService;
        private readonly IAuthenticationService _authenticationService;
        //private readonly IMediaPickerService _mediaPickerService;

        public UserProfile Profile
        {
            get
            {
                return _profile;
            }

            set
            {
                _profile = value;
                RaisePropertyChanged(() => Profile);
            }
        }
        
        /// <summary>
        /// TODO remove implementation and replace with DI.
        /// default constructor for the moment.
        /// </summary>
        /// <param name="page"></param>
        public ProfileViewModel(Page page)
        {
            this.CurrentPage = page;
            this._profileService = new ProfileService();
        }


        public ICommand LogoutCommand => new Command(OnLogout);

       // public ICommand UpdatePhotoCommand => new Command(OnUpdatePhoto);

        //public ProfileViewModel(IProfileService profileService, IAuthenticationService authenticationService)
        //{
        //    _profileService = profileService;
        //    _authenticationService = authenticationService;
        //    //_mediaPickerService = mediaPickerService;
        //}

        public  async Task InitializeAsync(object navigationData)
        {
            IsBusy = true;

            try
            {
                var profile = string.IsNullOrEmpty(Settings.UserId)?   await _profileService.GetFacebookProfileAsync()  :
                    await _profileService.GetCurrentProfileAsync(Settings.UserId);
                
                //if (!string.IsNullOrEmpty(profile.PhotoUrl))
                //{
                //    // Force photo reload
                //    profile.PhotoUrl += $"?t={DateTime.Now.Ticks}";
                //}
                
                    Profile = profile;
                 
              
                MessagingCenter.Send(Profile, MessengerKeys.ProfileUpdated);
            }
            catch (Exception ex) when (ex is WebException || ex is HttpRequestException)
            {
                await DialogService.ShowAlertAsync("Communication error", "Error", "Ok");
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"Error fetching profile with exception: {ex}");
            }

            IsBusy = false;
        }

        private async void OnLogout(object obj)
        {
            await _authenticationService.LogoutAsync();
            await CurrentPage.Navigation.PushAsync(new SocialMediaLoginOption());
        }

        //private async void OnUpdatePhoto(object obj)
        //{
        //    IsBusy = true;

        //    try
        //    {
        //        string imageAsBase64 = await _mediaPickerService.PickImageAsBase64String();
        //        await _profileService.UploadUserImageAsync(Profile, imageAsBase64);

        //        Profile = null;
        //        await InitializeAsync(null);
        //    }
        //    catch (Exception ex) when (ex is WebException || ex is HttpRequestException)
        //    {
        //        await DialogService.ShowAlertAsync("Communication error", "Error", "Ok");
        //    }
        //    catch (Exception ex)
        //    {
        //        Debug.WriteLine($"Error uploading profile image with exception: {ex}");
        //    }

        //    IsBusy = false;
        //}
    }
}
