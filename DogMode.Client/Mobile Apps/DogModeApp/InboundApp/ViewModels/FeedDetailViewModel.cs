﻿using DogModeApp.Clients.Core.Services;
using DogModeApp.Clients.Core.Services.Interfaces;
using DogModeApp.Clients.Core.Helpers;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using DogMode.Models;

namespace DogModeApp.Clients.Core.ViewModels
{
    public class FeedDetailViewModel : BaseViewModel
    {

        

        public FeedDetailViewModel(Page page, int feedId)
        {
            this.CurrentPage = page;

            _ApointmentService = new AppointmentService();

            Initialize(appointmentId: feedId);
        }

        public FeedDetailViewModel(Page page, Appointment appointment)
        {
            this.CurrentPage = page;

            _ApointmentService = new AppointmentService();

            Initialize(appointment:appointment);
        }

        private void Initialize(Appointment appointment =null, int appointmentId=0)
        {
            IsBusy = true;

            if (appointment != null)
            {
                Appointment = appointment;
                IsBusy = false;
                return;
            }


            if (appointmentId != 0)
            {
                Task.Run(async () =>
                {
                    var ap = await _ApointmentService.GetById(appointmentId);

                    Device.BeginInvokeOnMainThread(() =>
                    {
                        Appointment = ap;
                        IsBusy = false;
                    });

                });
            }

        }

        private async Task LoadDetailsAsync(int feedId, string userid)
        {
            Appointment = await _ApointmentService.GetById(feedId);
            
            IsBusy = false;
        }

        public Appointment Appointment
        {
            get
            {
                return _appointment;
            }

            set
            {
                _appointment = value;
                RaisePropertyChanged(() => Appointment);
            }
        }

        private Appointment _appointment { get; set; } = null;
        private readonly IAppointmentService _ApointmentService;


    }
}
