﻿using DogModeApp.Clients.Core.Services;
using DogModeApp.Clients.Core.Services.Interfaces;
using DogModeApp.Clients.Core.Helpers;
using DogModeApp.Clients.Core.Models;
using DogModeApp.Clients.Core.Services.Interfaces;
using DogModeApp.Clients.Core.Validations;
using DogModeApp.Clients.Core.ViewModels.Base;
using DogModeApp.Clients.Core.Views;

using Microsoft.WindowsAzure.MobileServices;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using DogMode.Models;

namespace DogModeApp.Clients.Core.ViewModels
{
    public class LoginViewModel : BaseViewModel
    {
        private ValidatableObject<string> _userName;
        private ValidatableObject<string> _password;
        private bool _isValid;
        private bool _isEnabled;
        private readonly IAuthenticationService _authenticationService;
        private readonly IProfileService _profileService;

        //public LoginViewModel(IAuthenticationService authenticationService,IDialogService dialogService):base(dialogService)
        //{
        //    _authenticationService = authenticationService;
        //    _userName = new ValidatableObject<string>();
        //    _password = new ValidatableObject<string>();

        //    AddValidations();
        //}

        public LoginViewModel(Page page)
        {
            this.CurrentPage = page;
            this._authenticationService= new AuthenticationService();
            this._profileService = new ProfileService();

            _userName = new ValidatableObject<string>();
            _password = new ValidatableObject<string>();
            AddValidations();
        }


        public ValidatableObject<string> UserName
        {
            get
            {
                return _userName;
            }
            set
            {
                _userName = value;
                RaisePropertyChanged(() => UserName);
            }
        }

        public ValidatableObject<string> Password
        {
            get
            {
                return _password;
            }
            set
            {
                _password = value;
                RaisePropertyChanged(() => Password);
            }
        }

        public bool IsValid
        {
            get
            {
                return _isValid;
            }
            set
            {
                _isValid = value;
                RaisePropertyChanged(() => IsValid);
            }
        }

        public bool IsEnabled
        {
            get
            {
                return _isEnabled;
            }
            set
            {
                _isEnabled = value;
                RaisePropertyChanged(() => IsEnabled);
            }
        }

        public ICommand SignInCommand => new Command(SignInAsync);

        public ICommand GoToSignUpCommand => new Command(GoToSignUp);

        public ICommand ValidateCommand
        {
            get { return new Command(() => Enable()); }
        }


        public ICommand FacebookAuthCommand
        {
            get
            {
                 
                return _facebookAuthCommand ??
                  (_facebookAuthCommand = new Command(async () => await AuthenticationAction(MobileServiceAuthenticationProvider.Facebook)));
            }
        }
        private ICommand _facebookAuthCommand;

        public ICommand GoogleAuthCommand
        {
            get
            {
                return _googleAuthCommand ??
                  (_googleAuthCommand = new Command(async () => await AuthenticationAction(MobileServiceAuthenticationProvider.Google)));
            }
        }
        private ICommand _googleAuthCommand;

        private async void SignInAsync()
        {
            IsBusy = true;
            IsValid = true;
            bool isValid = Validate();
            bool isAuthenticated = false;

            if (isValid)
            {
                try
                {
                    isAuthenticated = await _authenticationService.LoginAsync(UserName.Value, Password.Value);
                }
                catch (ServiceAuthenticationException)
                {
                    await DialogService.ShowAlertAsync("Invalid credentials", "Login failure", "Try again");
                }
                catch (Exception ex) when (ex is WebException || ex is HttpRequestException)
                {
                    Debug.WriteLine($"[SignIn] Error signing in: {ex}");
                    await DialogService.ShowAlertAsync("Communication error", "Error", "Ok");
                }
                catch (Exception ex)
                {
                    Debug.WriteLine($"[SignIn] Error signing in: {ex}");
                }
            }
            else
            {
                IsValid = false;
            }

            if (isAuthenticated)
            {
                //await NavigationService.NavigateToAsync<MainViewModel>();
                await CurrentPage.Navigation.PushAsync(new HomePage());
            }

            IsBusy = false;
        }

        private async void GoToSignUp()
        {
            //await NavigationService.NavigateToAsync<SignUpViewModel>();
        }

        private bool Validate()
        {
            bool isValidUser = _userName.Validate();
            bool isValidPassword = _password.Validate();

            return isValidUser && isValidPassword;
        }

        private void Enable()
        {
            IsEnabled = !string.IsNullOrEmpty(UserName.Value) &&
                !string.IsNullOrEmpty(Password.Value);
        }

        private void AddValidations()
        {
            _userName.Validations.Add(new IsNotNullOrEmptyRule<string> { ValidationMessage = "Username should not be empty" });
            _password.Validations.Add(new IsNotNullOrEmptyRule<string> { ValidationMessage = "Password should not be empty" });
        }

       


        public async Task AuthenticationAction(MobileServiceAuthenticationProvider type)
        {

            IsBusy = true;
            bool isValid = false;
            bool isAuthenticated = false;

            try
            {

                if (App.Authenticator != null)
                {
                    isAuthenticated = await App.Authenticator.AuthenticateAsync(type);
                }


            }
            catch (InvalidOperationException ex)
            {
                if (ex.Message.Contains("Authentication was cancelled"))
                {
                    await DialogService.ShowAlertAsync("Authentication cancelled by the user", "Error", "Ok");
                }
            }
            catch (Exception)
            {
                await DialogService.ShowAlertAsync("Authentication failed", "Error", "Ok");
            }

           
            if (isAuthenticated)
            {

                var profile = await _profileService.GetCurrentProfileAsync(Settings.UserId);

                // var claims = await _profileService.GetUserClaims();

                Settings.UserId = profile.UserId;

                //send user profile to save into database
                await _profileService.AddAsync(profile);
                

                await CurrentPage.Navigation.PopToRootAsync();

                MessagingCenter.Send<UserProfile>(profile, MessengerKeys.ProfileUpdated);
            }

            IsBusy = false;
        }
    }
}
