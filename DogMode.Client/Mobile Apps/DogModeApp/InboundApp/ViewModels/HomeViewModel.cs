﻿using DogModeApp.Clients.Core.Services;
using DogModeApp.Clients.Core.Services.Interfaces;
using DogModeApp.Clients.Core.Helpers;
using DogModeApp.Clients.Core.Models;
using DogModeApp.Clients.Core.Views;
using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using DogMode.Models;

namespace DogModeApp.Clients.Core.ViewModels
{
    public class HomeViewModel : BaseViewModel
    {
        private readonly IWeatherService _weatherService;
        private readonly IProfileService _profileService;

        private readonly IAppointmentService _feedService;

        DateTime currentDate;

        public HomeViewModel(Page page)
        {
            _weatherService = new OpenWeatherMapService();
            _profileService = new ProfileService();
            _feedService = new AppointmentService();
            
            
            IsBusy = true;
              
            CurrentPage = page;

            currentDate = DateTime.Now;
            location = "San salvador";

            Device.BeginInvokeOnMainThread(async()=> await InitializeAsync());

            
            
        }

        private async Task InitializeAsync()
        {
            if (Settings.IsAuthenticated)
            {
               await LoadAppointmentsAsync();
            }
            else await this.CurrentPage.Navigation.PushAsync(new LoginPage());
        }
        


        private async Task LoadAppointmentsAsync()
        {
            IsBusy = true;
            /// TODO Remove hardcoded Beacon Id and integrate with Beacon detection service. 
            var appointments = await _feedService.GetCurrentAppointments();

            
            Appointments = new ObservableCollection<Appointment>() ;
            foreach (var app in appointments)
            {
                Appointments.Add(app);
            }

            IsBusy = false; 
        }


        public DateTime CurrentDate
        {
            get
            {
                return currentDate;
            }

            set
            {
                currentDate = value;
                RaisePropertyChanged(() => CurrentDate);
            }
        }

        string location = "-";

        public string Location
        {
            get
            {
                return location;
            }

            set
            {
                location = value;
                RaisePropertyChanged(() => Location);
            }
        }

        string temp = "-";

        public string Temp
        {
            get
            {
                return temp;
            }

            set
            {
                temp = value;
                RaisePropertyChanged(() => Temp);
            }
        }
 

        private ObservableCollection<Appointment> _appointments = new ObservableCollection<Appointment>();

        public ObservableCollection<Appointment> Appointments
        {
            get
            {
                return _appointments;
            }

            set
            {
                _appointments = value;
                RaisePropertyChanged(() => Appointments);
            }
        }

        public ICommand ShowEventCommand => new Command<Event>(ShowEventAsync);

        public ICommand ShowCustomRideCommand => new Command(CustomRideAsync);

        public ICommand ShowRecommendedRideCommand => new Command<Appointment>(RecommendedRideAsync);

        public ICommand RefreshCommand => new Command(async()=> await InitializeAsync());

        private async void ShowEventAsync(Event @event)
        {
           // await this.CurrentPage.Navigation.PushAsync(new FeedDetail(@event.Id));

        }

        private async void CustomRideAsync()
        {
            //await NavigationService.NavigateToAsync<CustomRideViewModel>();
            await Task.FromResult(false);
        }

        private async void RecommendedRideAsync(object obj)
        {
            var feed = obj as Appointment;

            //await NavigationService.NavigateToAsync<CustomRideViewModel>(parameters);
            if (feed != null)
                await this.CurrentPage.Navigation.PushAsync(new FeedDetail(feed));
        }
    }
}
