﻿using DogModeApp.Clients.Core.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DogModeApp.Clients.Core.Models;
using System.Net.Http;
using DogModeApp.Clients.Core.Services;
using DogModeApp.Clients.Core.Helpers;
using DogMode.Models;

namespace DogModeApp.Clients.Core.Services
{
    public class ProfileService : IProfileService
    {
        private readonly IRequestProvider _requestProvider;
        private readonly IAuthenticationService _authenticationService;

        //private LocalDatabaseService DataService;
        private string BaseUrl
        {

            get
            {
                var builder = new UriBuilder(GlobalSettings.ApiEndpoint);
                builder.Path = $"api";

                return builder.ToString();
            }
        }



        public ProfileService(IRequestProvider requestProvider, IAuthenticationService authenticationService)
        {
            _requestProvider = requestProvider;
            _authenticationService = authenticationService;

        }

        public ProfileService()
        {
            // TODO replace with DI
            _requestProvider = new RequestProvider();
            _authenticationService = new AuthenticationService();
        }


        //public Task<UserProfile> GetCurrentProfileAsync()
        //{
        //    var userId = _authenticationService.GetCurrentUserId();

        //    return _requestProvider.GetAsync<UserProfile>($"{BaseUrl}/profiles/{userId}");
        //}

        public Task<UserAndProfileModel> SignUp(UserAndProfileModel profile)
        {
            return _requestProvider.PostAsync<UserAndProfileModel>($"{BaseUrl}/profiles", profile);
        }

        public async Task UploadUserImageAsync(UserProfile profile, string imageAsBase64)
        {
            var userId = _authenticationService.GetCurrentUserId();

            var imageModel = new ImageModel
            {
                Data = imageAsBase64
            };

            await _requestProvider.PutAsync($"{BaseUrl}/profiles/image/{userId}", imageModel);
         
        }

        public async Task<UserProfile> GetFacebookProfileAsync( )
        {
            try
            {

               var profile = await AzureClientServiceManager.DefaultManager.CurrentClient.InvokeApiAsync<UserProfile>("UserInfo", HttpMethod.Get, null);

                return profile;
            }
            catch(Exception ex)
            {
                  return null;
            }
        }


        public Task<List<string>> GetUserClaims()
        {
            try
            {

                return AzureClientServiceManager.DefaultManager.CurrentClient.InvokeApiAsync<List<string>>("claims", HttpMethod.Get, null);
            }
            catch(Exception ex)
            {
                return Task.FromResult(new List<string>());
            }
        }

        public async Task<UserProfile> GetCurrentProfileAsync(string id)
        {
            try
            {

                var profile=  await _requestProvider.GetAsync<UserProfile>($"{BaseUrl}/userprofile/Get?id={id}");
                return profile;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public async Task AddAsync(UserProfile profile)
        {
            try
            {

                await _requestProvider.PostAsync<UserProfile>($"{BaseUrl}/userprofile/Add", profile);
            }
            catch (Exception ex)
            {

            }
        }
    }
}
