﻿using System.Threading.Tasks;

namespace DogModeApp.Clients.Core.Services.Interfaces
{
    public interface IAuthenticationService
    {
        bool IsAuthenticated { get; }

        Task<bool> LoginAsync(string userName, string password);

        Task LogoutAsync();

        string GetCurrentUserId();
    }
}
