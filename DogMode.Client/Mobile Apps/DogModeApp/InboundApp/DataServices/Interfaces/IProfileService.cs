﻿using DogMode.Models;
using DogModeApp.Clients.Core.Models;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DogModeApp.Clients.Core.Services.Interfaces
{
    public interface  IProfileService
    {
        Task<UserProfile> GetCurrentProfileAsync(string id);

        Task<UserProfile> GetFacebookProfileAsync();
        Task AddAsync(UserProfile profile);

        Task<List<string>> GetUserClaims();

        //Task<UserAndProfileModel> SignUp(UserAndProfileModel profile);

        //Task UploadUserImageAsync(UserProfile profile, string imageAsBase64);
    }
}
