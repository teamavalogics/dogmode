﻿using DogModeApp.Clients.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DogModeApp.Clients.Core.Views
{

    [XamlCompilation(XamlCompilationOptions.Skip)]
    public partial class MenuPage : ContentPage
    {
        public MenuPage(Action<DogModeApp.Clients.Core.Models.MenuItem> navigate)
        {
            InitializeComponent();
            BindingContext = new MenuViewModel(this,navigate);
        }
    }

    //class MenuPageViewModel : INotifyPropertyChanged
    //{

    //    public MenuPageViewModel()
    //    {
    //        IncreaseCountCommand = new Command(IncreaseCount);
    //    }

    //    int count;

    //    string countDisplay = "You clicked 0 times.";
    //    public string CountDisplay
    //    {
    //        get { return countDisplay; }
    //        set { countDisplay = value; OnPropertyChanged(); }
    //    }

    //    public ICommand IncreaseCountCommand { get; }

    //    void IncreaseCount() =>
    //        CountDisplay = $"You clicked {++count} times";


    //    public event PropertyChangedEventHandler PropertyChanged;
    //    void OnPropertyChanged([CallerMemberName]string propertyName = "") =>
    //        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

    //}
}
