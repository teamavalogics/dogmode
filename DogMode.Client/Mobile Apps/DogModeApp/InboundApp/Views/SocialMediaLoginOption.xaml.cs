﻿using DogModeApp.Clients.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DogModeApp.Clients.Core.Views
{

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SocialMediaLoginOption : ContentPage
    {
        public SocialMediaLoginOption()
        {
            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();
            this.BindingContext = new LoginViewModel(this);
        }


        protected override void OnBindingContextChanged()
        {
            base.OnBindingContextChanged();
        }
    }

    

    
}
