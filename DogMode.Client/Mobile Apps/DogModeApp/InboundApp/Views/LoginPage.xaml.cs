﻿using DogModeApp.Clients.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DogModeApp.Clients.Core.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPage : ContentPage
    {
        public LoginPage()
        {
             NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();

             this.BindingContext = new LoginViewModel(this);
        }
        protected override void OnBindingContextChanged()
        {
            base.OnBindingContextChanged();
        }
    }
}
