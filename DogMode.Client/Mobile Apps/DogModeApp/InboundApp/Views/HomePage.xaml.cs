﻿using DogMode.Models;
using DogModeApp.Clients.Core.Utils;
using DogModeApp.Clients.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DogModeApp.Clients.Core.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HomePage : ContentPage
    {
        public HomePage()
        {
            InitializeComponent();
            this.BindingContext = new HomeViewModel(this);

            // this.mainScrollView.Scrolled += ScrollView_Scrolled;
             
             
        }

        

        private void ScrollView_Scrolled(object sender, ScrolledEventArgs e)
        {
            var val = MathHelper.ReMap(e.ScrollY, ScrollMinLimit, ScrollMaxLimit, 1, 0);

            this.infoPanel.Scale = val;
            this.infoPanel.Opacity = val;
        }

        protected override void OnBindingContextChanged()
        {
            base.OnBindingContextChanged();

           // FeedsList.ListOrientation = Device.Idiom == TargetIdiom.Phone ? StackOrientation.Vertical : StackOrientation.Horizontal;
        }

        async void OnItemSelected(object sender, SelectedItemChangedEventArgs args)
        {
            var item = args.SelectedItem as Appointment;
            if (item == null || item.Id==0)
                return;

            await Navigation.PushAsync(new FeedDetail(item));

            // Manually deselect item
            ItemsListView.SelectedItem = null;
        }
        private const int ScrollMinLimit = 0;
        private const int ScrollMaxLimit = 190;
    }
}
