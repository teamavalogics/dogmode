﻿using DogModeApp.Clients.Core.ViewModels;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DogModeApp.Clients.Core.Views
{

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProfilePage : ContentPage
    {
        public ProfilePage()
        {
            InitializeComponent();
            this.BindingContext = new ProfileViewModel(this);
        }
    }
}
