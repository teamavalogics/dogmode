﻿using DogMode.Models;
using DogModeApp.Clients.Core.Utils;
using DogModeApp.Clients.Core.ViewModels;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DogModeApp.Clients.Core.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FeedDetail : ContentPage
    {
        public FeedDetail()
        {
            InitializeComponent();
            this.mainScrollView.Scrolled += ScrollView_Scrolled;
        }

        public FeedDetail(int feedId)
        {
            InitializeComponent();

            this.BindingContext = new FeedDetailViewModel(this, feedId);
            this.mainScrollView.Scrolled += ScrollView_Scrolled;
        }

        public FeedDetail(Appointment appointment)
        {
            InitializeComponent();
           

            this.BindingContext = new FeedDetailViewModel(this, appointment);
            this.mainScrollView.Scrolled += ScrollView_Scrolled;
        }

        private void ScrollView_Scrolled(object sender, ScrolledEventArgs e)
        {
            var val = MathHelper.ReMap(e.ScrollY, ScrollMinLimit, ScrollMaxLimit, 1, 0);

            this.infoPanel.Scale = val;
            this.infoPanel.Opacity = val;
        }

        private const int ScrollMinLimit = 0;
        private const int ScrollMaxLimit = 190;
    }
}