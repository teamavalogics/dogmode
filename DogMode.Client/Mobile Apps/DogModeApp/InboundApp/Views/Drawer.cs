﻿using DogModeApp.Clients.Core.Services;
using DogModeApp.Clients.Core.Helpers;
using DogModeApp.Clients.Core.Models;
using DogModeApp.Clients.Core.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using DogMode.Models;

namespace DogModeApp.Clients.Core.Views
{
    public class Drawer: MasterDetailPage
    {
        public Drawer()
        {
            InitializeDrawer();
        }

        public Drawer(int appointmentId)
        {
            
           
           
            Device.BeginInvokeOnMainThread(async()=>
            {
                Title = "DogMode App Menu";

                // home page in drawer control as root page.
                var homepage = new HomePage();
                var navigationControl = new NavigationPage(homepage);

                NavigationPage.SetBackButtonTitle(homepage, "");

                Detail = navigationControl;
                Master = new MenuPage(NavigateTo);

                var _feedService = new AppointmentService();

                var feed = await _feedService.GetById(appointmentId);

                await Detail.Navigation.PushAsync(new FeedDetail(feed));

            });
        }
        
        private void InitializeDrawer()
        {
            Title = "DogMode App Menu"; 
            
            // home page in drawer control as root page.
            var homepage = new HomePage();
            var navigationControl = new NavigationPage(homepage); 

            NavigationPage.SetBackButtonTitle(homepage, "");

            Detail = navigationControl;
            Master = new MenuPage(NavigateTo);

            Task.Run(async () => await LoadProfileAsync());
            
        }
 


        public async void NavigateTo(DogModeApp.Clients.Core.Models.MenuItem menu)
        {
            if (menu == null)
                return;

            // this.Title = "Regresar";
            Page displayPage = (Page)Activator.CreateInstance(menu.ViewModelType);
            var navigation = Detail as NavigationPage;

            await Detail.Navigation.PushAsync(displayPage);

            //if (typeof(InfoTabs) == displayPage.GetType()) await ChangeSelectedTabForInfoTabs(navigation, (displayPage as InfoTabs), menu);
            //else
            //{

            //    //if (navigation.Navigation.NavigationStack.Any(i => i.GetType() == menu.TargetType)
            //    //    && menu.TargetType != navigation.CurrentPage.GetType())
            //    //{
            //    //    await Detail.Navigation.PopAsync();
            //    //    IsPresented = false;
            //    //    return;
            //    //};


            //    if (menu.TargetType != navigation.CurrentPage.GetType())
            //    {
            //        await Detail.Navigation.PushAsync(displayPage);

            //    }
            //} 

            IsPresented = false;


        }
     
        private async Task LoadProfileAsync()
        {
            var _profileService = new ProfileService();
            var profile = await _profileService.GetFacebookProfileAsync();

            if (profile == null || string.IsNullOrEmpty(profile.UserId))
            {
                profile = await _profileService.GetCurrentProfileAsync(Settings.UserId);
            }

            MessagingCenter.Send<UserProfile>(profile, MessengerKeys.ProfileUpdated);
        }
    }
}
