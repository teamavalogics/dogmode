using System;

using GalaSoft.MvvmLight;
using System.Threading.Tasks;

namespace DogModeApp.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        public MainViewModel()
        {

            Task.Run(()=> UpdateClock());
        }

        public string Clock
        {
            get { return _clock; }
            set
            {
                _clock = value;
                Set(ref _clock, value);
            }
        }

        private void UpdateClock()
        {
            while (true)
            {
                Task.Delay(TimeSpan.FromSeconds(1));
                
                Clock = DateTime.Now.TimeOfDay.ToString();
            }
        }

        #region Private Properties




        private string _clock = string.Empty;
        #endregion
    }
}
