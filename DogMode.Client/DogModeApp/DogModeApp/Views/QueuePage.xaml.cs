using DogModeApp.ViewModels;

using Windows.UI.Xaml.Controls;

namespace DogModeApp.Views
{
    public sealed partial class QueuePage : Page
    {
        private QueueViewModel ViewModel
        {
            get { return DataContext as QueueViewModel; }
        }

        public QueuePage()
        {
            InitializeComponent();
        }
    }
}
