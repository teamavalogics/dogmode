﻿using ApiClient;
using ApiClient.HubClient;
using ApiClient.HubClient.Interfaces;
using DogMode.Models;
using DogModeApp.Clients.Core.Services;
using DogModeApp.Clients.Core.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;

namespace DogModeApp.WPF
{
    public class MainViewModel : BaseViewModel
    {

        public MainViewModel()
        {
            // TODO use dependency Injection for that.
             HubClient = new NotifyHubServiceClient($"{Settings.CurrentDeviceId}");
            AppointmentService = new AppointmentService();
            // HubClient.OnGetConnectedDevices += FillDevicesList;
            HubClient.OnGetActions += async ()=> await GetAppointmentsAsync();
            // add new appointment action
            HubClient.OnNewAppointmentAdded += (appoitment) => SetNewAppointMents(new List<Appointment>() { appoitment }); 

            // for admin purposes

            Status = "Connecting with Hub....";

            Task.Run(() => SetClock());
            Task.Run(async () =>

            {
                IsBusy = true;
                Status = await HubClient.ConnectAsync();
               
                IsBusy = false;
               
            });
            Task.Run(async () => await GetAppointmentsAsync());
        }

     

        public ObservableCollection<Appointment> Appointments
        {
            get { return _appointments ?? (_appointments = new ObservableCollection<Appointment>()); }

            set
            {
                _appointments = value;
                RaisePropertyChangedEvent("Appointments");
            }
        }
 

        public string Status
        {
            get { return status; }
            set
            {
                status = value;
                RaisePropertyChangedEvent("Status");
            }
        }

        public string Clock
        {
            get { return _clock; }
            set
            {
                _clock = value;
                RaisePropertyChangedEvent("Clock");
            }
        }



        public ICommand RefreshCommand
        {
            get
            {
                return _refreshCommand ??
                    (_refreshCommand = new CommandHandler((param) => HubClient.GetDevices(), true));
            }
        }

        private void SetNewAppointMents(List<Appointment> appointments)
        {
            var newAppointments = appointments.Where(appoint => !_appointments.Any(d => d.Id == appoint.Id)).ToList();

            var collection = _appointments.ToList().Union(newAppointments).ToList();

            Appointments = new ObservableCollection<Appointment>(collection);
        }

        private void FillAppointmentsList(List<Appointment> appointments)
        {

            Dispatcher.CurrentDispatcher.Invoke(() =>SetNewAppointMents(appointments));
        }

        private async Task GetAppointmentsAsync()
        {
            var filter = new FilterOptionModel()
            {
                date = DateTime.Now.Date,
                end = DateTime.Now.Date.AddDays(1),
            };

            var appointments = await AppointmentService.GetCurrentAppointments(filter);

            Dispatcher.CurrentDispatcher.Invoke(() => SetNewAppointMents(appointments));

        }

        private void SetClock()
        {
            while (true)
            {
                Task.Delay(TimeSpan.FromSeconds(1));

                Clock = string.Format("{0:HH:mm:ss}", DateTime.Now);
            }
        }


        private ObservableCollection<Appointment> _appointments = null;
        

        private string status { get; set; }
        private string _clock { get; set; }

        private CommandHandler _GetAvailableActionsComment = null;

        private CommandHandler _refreshCommand = null;

        INotifyService HubClient { get; set; }

        IAppointmentService AppointmentService { get; set; }

    }
}
