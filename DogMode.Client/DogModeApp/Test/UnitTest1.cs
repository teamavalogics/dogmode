﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DogMode.Services;
using DogMode.Models;
using System.Threading.Tasks;

namespace Test
{
    [TestClass]
    public class BasicApiTestIntegration
    {

        private IAppointmentService AppointmentService { get; set; }


        [TestInitialize]
        public void Init()
        {
            AppointmentService = new AppointmentsService();
        }

        [TestMethod]
        public async Task SearchAppointmentsAsync()
        {

            var filter = new FilterOptionModel()
            {
                date = DateTime.Now.Date,
                end =DateTime.Now.Date.AddDays(1),
            };

            var result = await AppointmentService.Search(filter);

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task SearchAppointmentsSimple()
        {

            var result = await AppointmentService.Search();

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task SearchByDate()
        {
            var result = await AppointmentService.Search(DateTime.Now.Date);

            Assert.IsNotNull(result);
        }
    }
}
