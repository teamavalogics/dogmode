﻿using DogMode.Models;
using DogMode.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DogMode
{

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            this.BindingContext = new HomeViewModel(this, AppointmentType.KennelDayCare,ScrollTo);


        }

        private void ScrollTo(object item)
        {
            Device.BeginInvokeOnMainThread(() => this.ItemsListView.ScrollTo(item, ScrollToPosition.Start, true));
        }
    }
}
