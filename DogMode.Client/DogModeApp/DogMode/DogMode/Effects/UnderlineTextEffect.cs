﻿using Xamarin.Forms;

namespace DogMode.Effects
{
    public class UnderlineTextEffect : RoutingEffect
    {
        public UnderlineTextEffect() : base("DogMode.UnderlineTextEffect")
        {
        }
    }
}
