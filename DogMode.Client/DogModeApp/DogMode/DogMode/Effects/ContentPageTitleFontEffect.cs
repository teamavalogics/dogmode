﻿using Xamarin.Forms;

namespace DogMode.Effects
{
    public class ContentPageTitleFontEffect : RoutingEffect
    {
        public ContentPageTitleFontEffect() : base("DogMode.ContentPageTitleFontEffect")
        {
        }
    }
}
