﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DogMode
{
    public static class GlobalSettings
    {
        public const string AuthenticationEndpoint = "https://inbounddata-service.azurewebsites.net/";

        public const string ApiEndpoint = "http://dogmode.azurewebsites.net";

        //public const string ApiEndpoint = "http://localhost:44302/";

        public const string OpenWeatherMapAPIKey = "YOUR_WEATHERMAP_API_KEY";

        public const string HockeyAppAPIKeyForAndroid = "5f747f0c300d44e582a7965e615606f4";
        public const string HockeyAppAPIKeyForiOS = "YOUR_HOCKEY_APP_ID";

        public const string SkypeBotAccount = "skype:YOUR_BOT_ID?chat";

        public const string BingMapsAPIKey = "YOUR_BINGMAPS_API_KEY";

        public static string City => "New York City";

        public static object HubName = "NotifyHub";

        public static int TenantId = 1;
    }
}
