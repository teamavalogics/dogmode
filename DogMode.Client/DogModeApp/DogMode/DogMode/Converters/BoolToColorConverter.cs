﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace DogMode.Converters
{
    public class BoolToColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                var val = (DateTime)value;

                var date = DateTime.Now;

                if (date >= val) return Color.DarkRed;

                var diff = (TimeSpan)(val - date);
                return (diff.TotalMinutes>0 &&   diff.Minutes <= 5) ? Color.DarkOrange : Color.Gray;
                
            }

            return Color.Gray;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
