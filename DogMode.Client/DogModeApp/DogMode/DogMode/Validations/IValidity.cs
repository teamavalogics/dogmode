﻿namespace DogMode.Validations
{
    public interface IValidity
    {
        bool IsValid { get; set; }
    }
}
