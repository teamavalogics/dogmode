﻿using DogMode.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DogMode.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OpenSwimView : ContentPage
    {
        public OpenSwimView()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            this.BindingContext = new HomeViewModel(this,Models.AppointmentType.OpenSwim,ScrollTo);
        }


        private void ScrollTo(object item)
        {
            Device.BeginInvokeOnMainThread(()=> this.ItemsListView.ScrollTo(item, ScrollToPosition.Start, true));
        }
       

        private void ItemsListView_ItemAppearing(object sender, ItemVisibilityEventArgs e)
        {

        }
    }
}