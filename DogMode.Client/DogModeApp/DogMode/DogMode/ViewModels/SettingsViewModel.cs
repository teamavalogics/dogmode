﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DogMode.Views;
using Xamarin.Forms;
using DogMode.Helpers;
using System.Windows.Input;

namespace DogMode.ViewModels
{
    public class SettingsViewModel : BaseViewModel
    {
        

        public SettingsViewModel(Page settingsView)
        {
            this.CurrentPage = settingsView;

            _isSelected = Settings.OpenSwimAsDefault;
            _scrollDelay = Settings.ScrollDelay;
            _syncCheckDelay = Settings.SyncHealthCheckDelay;
        }


        public bool isOn
        {
            get { return _isSelected; }
            set
            {
                Settings.OpenSwimAsDefault=_isSelected = value;
                RaisePropertyChanged(() => isOn);
            }
        }

        public int ScrollDelay
        {
            get { return _scrollDelay; }
            set
            {
                _scrollDelay= Settings.ScrollDelay = value;
                RaisePropertyChanged(() => ScrollDelay);
            }
        }

        public int SyncHealthCheckDelay
        {
            get { return _syncCheckDelay; }
            set
            {
                _syncCheckDelay = Settings.SyncHealthCheckDelay = value;
                RaisePropertyChanged(() => SyncHealthCheckDelay);
            }
        }

        public ICommand GoBackCommand => new Command(async ()=> await GoBackAction());

        async Task GoBackAction()
        {
            //if (isOn)   await CurrentPage.Navigation.PushAsync(new OpenSwimView());

            //else await CurrentPage.Navigation.PushAsync(new MainPage());
            await CurrentPage.Navigation.PopToRootAsync();
        }

        #region private properties

        private bool _isSelected { get; set; }

        private int _scrollDelay { get; set; }

        private int _syncCheckDelay { get; set; }

        #endregion
    }
}
