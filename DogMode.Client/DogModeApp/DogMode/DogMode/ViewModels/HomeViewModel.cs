﻿using DogMode.Helpers;
using DogMode.Models;
using DogMode.Services;
using DogMode.Services.Interfaces;
using DogMode.Views;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace DogMode.ViewModels
{
    public class HomeViewModel : BaseViewModel
    {
        private readonly IAppointmentService _appointmentService;
        int ActivePage = 0;
        int ItemsPerPage = 5;
        public HomeViewModel(Page page, AppointmentType appointmentType, Action<object> scrollto)
        {
            AppointmentType = appointmentType;

            this.CurrentPage = page;
            _appointmentService = new AppointmentsService();

            Task.Run(async () => await ClockLoopAsync());

            //Device.BeginInvokeOnMainThread(async () => await InitializeAsync());

            Task.Factory.StartNew(async () => await InitializeAsync());
            Task.Factory.StartNew(async () => await InitializeSyncHubAsync());
            Task.Factory.StartNew(async () =>
            {
                while (true)
                {
                    await Task.Delay(TimeSpan.FromSeconds(60));

                    Appointments = new ObservableCollection<AppointmentViewModel>(_appointments.ToList());
                }
            });

            ScrollTo = scrollto;

            Task.Factory.StartNew(async () => await SycnCheck());

            Task.Factory.StartNew(async () => await ScroolLoopAsync());

        }

        private async Task InitializeAsync()
        {
            IsBusy = true;


            Clock = DateTime.Now.ToString("H:mm:ss tt");

            await LoadAppointmentsAsync();

            if (Settings.ScrollDelay == 0) Settings.ScrollDelay = 2;
            if (Settings.SyncHealthCheckDelay == 0) Settings.SyncHealthCheckDelay = 300;
        }

        private async Task InitializeSyncHubAsync()
        {
            //instantiate the hub and events. 
            NotifyHubService = new NotifyHubServiceClient(useRandomId: true);
            IsConnectingToHub = true;
            NotifyHubService.OnConnected += () => IsConnectingToHub = false;
            NotifyHubService.OnConnecting += () => IsConnectingToHub = true;

            // Sync Callback
            NotifyHubService.OnSync += async (model) =>
            {
                await Sync(model);
            };

            //start hub connection .
            IsConnectingToHub = !((await NotifyHubService.ConnectAsync()) == "OK");


            NotifyHubService.CheckConnectionStatus();
        }

        private async Task LoadAppointmentsAsync()
        {
            IsBusy = true;

            var appointments = await _appointmentService.Search(AppointmentType);


            Appointments = new ObservableCollection<AppointmentViewModel>(appointments);
            AppointmentsShowed = new ObservableCollection<AppointmentViewModel>(appointments.Take(ItemsPerPage));

            IsBusy = false;

        }


        private async Task RefreshAppointments()
        {
            var appointments = await _appointmentService.Search(AppointmentType);

            var currentIds = Appointments.Select(a => a.Id).ToList();
            var newIds = appointments.Select(a => a.Id).ToList();

            var newAppointmentsIds = currentIds.Except(newIds).ToList();

            foreach (var app in appointments)
            {
                if (string.IsNullOrEmpty(app.Image))
                    app.Image = "Assets/dogicon.png";
            }

            if (newAppointmentsIds.Any() || appointments.Any())
            {
                Appointments = new ObservableCollection<AppointmentViewModel>(appointments);
            }
        }

        private async Task Sync(AppointmentViewModel model)
        {
            IsConnectingToHub = true;
            if (model == null)
            {
                await LoadAppointmentsAsync();
                IsConnectingToHub = false;
                return;
            }
            var appointments = _appointmentService.Sync(_appointments.ToList(), model, AppointmentType);
            Appointments = new ObservableCollection<AppointmentViewModel>(appointments);
            IsConnectingToHub = false;
        }

        #region Recursive Process
        public async Task ClockLoopAsync()
        {
            while (true)
            {
                Clock = DateTime.Now.ToString("h:mm:ss tt");
                await Task.Delay(TimeSpan.FromSeconds(1));
            }
        }

        private async Task ScroolLoopAsync()
        {
            while (true)
            {
                await Task.Delay(TimeSpan.FromSeconds(Settings.ScrollDelay));
                if(Appointments.Count > ItemsPerPage)
                {
                    ActivePage++;
                    if ((ActivePage * ItemsPerPage) > Appointments.Count)
                        ActivePage = 0;

                    AppointmentsShowed = new ObservableCollection<AppointmentViewModel>(Appointments.Skip(ActivePage * ItemsPerPage).Take(ItemsPerPage));
                }
            }
        }

        private async Task SycnCheck()
        {
            while (true)
            {
                await Task.Delay(TimeSpan.FromSeconds(Settings.SyncHealthCheckDelay));
                await Task.Factory.StartNew(async () => await RefreshAppointments());
            }
        }

        #endregion

        public string Clock
        {
            get
            {
                return _clock;
            }

            set
            {
                _clock = value;
                RaisePropertyChanged(() => Clock);
            }
        }


        public ObservableCollection<AppointmentViewModel> Appointments
        {
            get { return _appointments; }
            set
            {
                _appointments = value;
                RaisePropertyChanged(() => Appointments);
            }
        }

        public ObservableCollection<AppointmentViewModel> AppointmentsShowed
        {
            get { return _appointmentsShowed; }
            set
            {
                _appointmentsShowed = value;
                RaisePropertyChanged(() => AppointmentsShowed);
            }
        }

        public bool IsConnectingToHub
        {
            get { return _IsConnectingTohub; }
            set
            {
                _IsConnectingTohub = value; ;
                RaisePropertyChanged(() => IsConnectingToHub);
            }
        }

        #region Private Properties

        private string _clock { get; set; }

        private ObservableCollection<AppointmentViewModel> _appointments { get; set; }
        private ObservableCollection<AppointmentViewModel> _appointmentsShowed { get; set; }

        private bool _IsConnectingTohub { get; set; }
        #endregion  Private properties

        public ICommand GotoSettingsPageCommand => new Command(async () => await NavigateSettingsPage());

        async Task NavigateSettingsPage()
        {
            await CurrentPage.Navigation.PushAsync(new SettingsView());
        }

        private INotifyService NotifyHubService = null;


        AppointmentType AppointmentType { get; set; } = AppointmentType.KennelDayCare;

        private Action<object> ScrollTo = null;
    }
}
