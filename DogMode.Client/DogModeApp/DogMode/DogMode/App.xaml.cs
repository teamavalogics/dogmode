﻿using DogMode.Helpers;
using DogMode.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace DogMode
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            
            NavigationPage.SetHasNavigationBar(this, false);

            var navigationPage = Settings.OpenSwimAsDefault? new NavigationPage(new OpenSwimView()) : new NavigationPage(new MainPage());
            MainPage = navigationPage;
            
        }

        

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
