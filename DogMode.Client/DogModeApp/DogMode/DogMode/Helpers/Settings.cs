// Helpers/Settings.cs
using Plugin.Settings;
using Plugin.Settings.Abstractions;

namespace DogMode.Helpers
{
	/// <summary>
	/// This is the Settings static class that can be used in your Core solution or in any
	/// of your client applications. All settings are laid out the same exact way with getters
	/// and setters. 
	/// </summary>
	public static class Settings
	{
		private static ISettings AppSettings
		{
			get
			{
				return CrossSettings.Current;
			}
		}

		#region Setting Constants

		private const string SettingsKey = "settings_key";
		private static readonly string SettingsDefault = string.Empty;

        private const string DefaultView = "settings_defaultView";

        private const string ScrollDelayKey = "settings_scroll_delay"; // seconds

        private const string SyncDelayCheckKey = "settings_sync_health_check";
        #endregion

        public static bool OpenSwimAsDefault
        {
            get
            {
                return AppSettings.GetValueOrDefault(DefaultView, false);

            }
            set
            {
                AppSettings.AddOrUpdateValue(DefaultView, value);
            }
        }

        public static string GeneralSettings
		{
			get
			{
				return AppSettings.GetValueOrDefault(SettingsKey, SettingsDefault);
			}
			set
			{
				AppSettings.AddOrUpdateValue(SettingsKey, value);
			}
		}

        public static int ScrollDelay
        {
            get { return AppSettings.GetValueOrDefault(ScrollDelayKey, 3); }
            set { AppSettings.AddOrUpdateValue(ScrollDelayKey, value); }
        }

        public static int SyncHealthCheckDelay
        {
            get { return AppSettings.GetValueOrDefault(SyncDelayCheckKey, 10); }
            set { AppSettings.AddOrUpdateValue(SyncDelayCheckKey, value); }
        }
    }
}