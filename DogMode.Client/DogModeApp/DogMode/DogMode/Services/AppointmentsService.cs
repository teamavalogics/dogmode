﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DogMode.Models;

namespace DogMode.Services
{
    public class AppointmentsService :IAppointmentService
    {
        private readonly IRequestProvider _requestProvider;

        public AppointmentsService()
        {
            // TODO implement with dependendy injection
            _requestProvider = new RequestProvider();
        }

        private string BaseUrl
        {

            get
            {
                var builder = new UriBuilder(GlobalSettings.ApiEndpoint);
                builder.Path = $"api";

                return builder.ToString();
            }
        }

        public Task<Appointment> Get(int id)
        {
            try
            {
                return _requestProvider.GetAsync<Appointment>($"{BaseUrl}/appointments/get?id={id}");
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public Task<List<AppointmentViewModel>> Search()
        {
            try
            {
                return _requestProvider.GetAsync<List<AppointmentViewModel>>($"{BaseUrl}/appointments/search");
            }
            catch (Exception ex)
            {
                // todo log this
                return Task.FromResult(new List<AppointmentViewModel>());
            }
        }

        public Task<List<AppointmentViewModel>> Search(FilterOptionModel filter)
        {
            try
            {
                return _requestProvider.PostAsync<FilterOptionModel, List<AppointmentViewModel>>($"{BaseUrl}/appointments/search", filter);
            }
            catch (Exception ex)
            {
                // todo log this
                return Task.FromResult(new List<AppointmentViewModel>());
            }
        }

        public Task<List<AppointmentViewModel>> Search(DateTime date)
        {
            
            try
            {
                return _requestProvider.GetAsync< List<AppointmentViewModel>>($"{BaseUrl}/appointments/search?date={date}");
            }
            catch (Exception ex)
            {
                // todo log this
                return Task.FromResult(new List<AppointmentViewModel>());
            }
        }

        public Task<List<AppointmentViewModel>> Search(AppointmentType type)
        {
            try
            {
                return _requestProvider.GetAsync<List<AppointmentViewModel>>($"{BaseUrl}/appointments/search?type={type}");
            }
            catch (Exception ex)
            {
                // todo log this
                return Task.FromResult(new List<AppointmentViewModel>());
            }
        }

        public List<AppointmentViewModel> Sync(List<AppointmentViewModel> items, AppointmentViewModel item, AppointmentType type)
        {
            foreach (var app in items)
            {
                if(string.IsNullOrEmpty(app.Image))
                    app.Image = "Assets/dogicon.png";
            }

            // evaluate if   exists the estatus completed to remove 
            if (item != null && items.Any(i=> i.Id== item.Id) && item.Type== type && 
                item.Status == QueueStatus.Completed)
            {
                var toRemove = items.First(i => i.Id == item.Id);

                items.Remove(toRemove);

            }
            // add if not status is active to add
            if(item!=null && !items.Any(i=> i.Id== item.Id) && item.Type == type && 
                item.Status == QueueStatus.Active)
            {
                items.Add(item);
            }
            
                return items.OrderBy(i=> i.Checkout).ToList();
        }
    }
}
