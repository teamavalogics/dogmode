﻿using DogMode.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DogMode.Services
{
    public interface IAppointmentService
    {

        Task<Appointment> Get(int id);

        Task<List<AppointmentViewModel>> Search();

        Task<List<AppointmentViewModel>> Search(FilterOptionModel filter);

        Task<List<AppointmentViewModel>> Search(DateTime date);

        Task<List<AppointmentViewModel>> Search(AppointmentType type);

        List<AppointmentViewModel> Sync(List<AppointmentViewModel> items, AppointmentViewModel item, AppointmentType type);

    }
}
