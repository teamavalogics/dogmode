﻿using DogMode.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DogMode.Services.Interfaces
{
    public interface INotifyService
    {
        Task<string> ConnectAsync();

        void NotifyAvailableActions(string key);

        void TryReconnect();

        void GetDevices();

        void GetAvailableActions(string key);

        void CheckConnectionStatus();


        Action<dynamic>OnSync { get; set; }

        Action OnConnecting { get; set; }

        Action OnConnected { get; set; }

        Action<Appointment> OnNewAppointmentAdded { get; set; }

    }
}
