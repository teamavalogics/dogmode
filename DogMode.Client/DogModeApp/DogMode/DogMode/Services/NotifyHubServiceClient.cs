﻿using DogMode.Helpers;
using DogMode.Models;
using DogMode.Services.Interfaces;
using Microsoft.AspNet.SignalR.Client;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DogMode.Services
{
    public class NotifyHubServiceClient : INotifyService
    {

        public NotifyHubServiceClient(string customId = "", bool useRandomId = false)
        {


            var queryString = new Dictionary<string, string>();

            if (useRandomId)
                ConnectionId = Guid.NewGuid().ToString();

            if (!string.IsNullOrEmpty(customId))
                ConnectionId = customId;

            //to manage  specific connection id as key in Signal R connection manager
            queryString.Add("key", ConnectionId);

            // initialize Hub Connection.
            Connection = new HubConnection($"{GlobalSettings.ApiEndpoint}/{GlobalSettings.HubName}", queryString, useDefaultUrl: false);

        }



        /// <summary>
        /// Start connection purpose .
        /// </summary>
        /// <returns></returns>
        public async Task<string> ConnectAsync()
        {
            Connection.Closed += TryReconnect;
            Connection.Reconnected += OnConnected;
            Connection.Reconnecting += OnConnecting;

            Connection.StateChanged += OnStateConnectionChanged;



            HubProxy = Connection.CreateHubProxy($"{GlobalSettings.HubName}");

            //Handle incoming event from server: 
            HubProxy.On<object>("Sync",sync);
         
            //HubProxy.On("OnDeviceAddedCallBack", (device) =>
            //{

            //    var d = JsonConvert.DeserializeObject<Device>(device.ToString());
            //    GetDevicesCallBack(new List<Device>() { d });

            //});
            //HubProxy.On("GetDevicesCallBack", (devices) => 
            //{
            //    var _devices = JsonConvert.DeserializeObject<List<Device>>(devices.Root.ToString());
            //    GetDevicesCallBack( _devices);
            //});

            //HubProxy.On("NotifyAvailableActionsCallBack", (actions) =>
            //{
            //    var _actions = JsonConvert.DeserializeObject<List<NotificationModel>>(actions.Root.ToString());
            //    GetAvailableActionsCallback(_actions);
            //});

            try
            {
                await Connection.Start();
                return "OK";
            }
            catch (Exception ex)
            {
                TryReconnect();
                return "Unable to connect to server: Start server before connecting clients.";


            }
        }

      

        private void  sync(object model)
        {
            var item = model != null ? JsonConvert.DeserializeObject<AppointmentViewModel>(model.ToString()) : null;
            if (OnSync != null) OnSync(item);
        }
     

        public void TryReconnect()
        {
           
            
           lock (locker)
            {
                if (OnConnecting != null) OnConnecting();
                // to avoid multiple connection Retryl
                if (State == ConnectionState.Connected) return;

                Task.Run(async () =>
                {
                    while(await ConnectAsync() != "OK")
                    {
                        await Task.Delay(TimeSpan.FromSeconds(10));
                    }
                });
           }


        }

        public void OnStateConnectionChanged(StateChange state)
        {
            State = state.NewState;
            if (State == ConnectionState.Connected) OnConnected();
            if (State == ConnectionState.Disconnected)
            {
                TryReconnect();
                OnConnecting();
            }
        }

        public void CheckConnectionStatus()
        {
            Task.Factory.StartNew(async () =>
            {
                while (true)
                {

                    await Task.Delay(TimeSpan.FromSeconds(10));

                    if (State == ConnectionState.Connected) continue;

                    TryReconnect();

                }
                
            });
        }

        public void NotifyAvailableActions(string key)
        {
            throw new NotImplementedException();
        }

        public void GetDevices()
        {

        }

        public void GetAvailableActions(string key)
        {
            throw new NotImplementedException();
        }




        #region Private Properties
        private IHubProxy HubProxy { get; set; }



        private HubConnection Connection { get; set; }


        public string ConnectionId { get; set; } = "0001";
        public Action<dynamic> OnSync { get; set; } = null;

        public Action OnConnecting { get;   set; } = null;
        
        public Action<Appointment> OnNewAppointmentAdded { get; set; }
        public Action OnConnected { get;   set; } = null;

        private ConnectionState State { get; set; }

        private object locker { get; set; }

        #endregion

    }
}
