﻿using DogModeApp.Clients.Core.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using DogMode.Models;

namespace DogModeApp.Clients.Core.Services
{
    public class AppointmentService : IAppointmentService
    {
        private readonly IRequestProvider _requestProvider;

        public AppointmentService()
        {
            _requestProvider = new RequestProvider();
        }

        private string BaseUrl
        {

            get
            {
                var builder = new UriBuilder(GlobalSettings.ApiEndpoint);
                builder.Path = $"api";

                return builder.ToString();
            }
        }

        /// <summary>
        /// Get Ad By Id from Api
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<Appointment> GetById(int id)
        {
            try
            {
                return _requestProvider.GetAsync<Appointment>($"{BaseUrl}/appointments/get?id={id}");
            }
            catch(Exception ex)
            {
                return null;
            }
        }

        public Task<List<Appointment>> GetCurrentAppointments(FilterOptionModel filter)
        {
            try
            {
                return _requestProvider.PostAsync<FilterOptionModel,List<Appointment>>($"{BaseUrl}/appointments/filter",filter);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public Task<List<Appointment>> GetCurrentAppointments()
        {
            try
            {
                return _requestProvider.GetAsync<List<Appointment>>($"{BaseUrl}/appointments/get");
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
