﻿using DogMode.Models;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DogModeApp.Clients.Core.Services.Interfaces
{
    public interface    IAppointmentService
    {
        Task<List<Appointment>> GetCurrentAppointments(FilterOptionModel filter);
        Task<Appointment> GetById(int id);
        Task<List<Appointment>> GetCurrentAppointments();
    }
}
