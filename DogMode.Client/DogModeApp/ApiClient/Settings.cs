﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;

namespace ApiClient
{

    /// <summary>
    /// todo migrate to shared Logic
    /// </summary>
    public static class Settings
    {

        /// <summary>
        /// TODO implement Settings logic 
        /// </summary>
        public static string ApiUrl { get; set; } = ConfigurationManager.AppSettings["ApiUrl"];

        public static string HubName { get; set; } = "NotifyHub";

        public static string CurrentDeviceId { get; set; } = ConfigurationManager.AppSettings["DeviceId"];



        public const string AuthenticationEndpoint = "";

        public const string ApiEndpoint = "https://dogmode.azurewebsites.net";

        public const string OpenWeatherMapAPIKey = "YOUR_WEATHERMAP_API_KEY";

        public const string HockeyAppAPIKeyForAndroid = "5f747f0c300d44e582a7965e615606f4";
        public const string HockeyAppAPIKeyForiOS = "YOUR_HOCKEY_APP_ID";

        public const string SkypeBotAccount = "skype:YOUR_BOT_ID?chat";

        public const string BingMapsAPIKey = "YOUR_BINGMAPS_API_KEY";

        public static string City => "New York City";

        public static int TenantId = 1;
    }
}
