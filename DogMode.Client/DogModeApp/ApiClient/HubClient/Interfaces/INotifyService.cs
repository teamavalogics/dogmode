﻿
using DogMode.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ApiClient.HubClient.Interfaces
{
    public interface INotifyService
    {
        Task<string> ConnectAsync();

        void NotifyAvailableActions(string key);

        void ConnectionClosed();

        void GetDevices();

        void GetAvailableActions(string key);

        
        Action OnGetActions { get; set; }

        Action<Appointment> OnNewAppointmentAdded { get; set; }

    }
}
