﻿using Microsoft.AspNet.SignalR.Client;
using System;
using System.Collections.Generic;
using System.Text;

using System.Threading.Tasks;
using ApiClient.HubClient.Interfaces;
using System.Net.Http;
 
using System.Linq;
using Newtonsoft.Json;
using DogMode.Models;

namespace ApiClient.HubClient
{
    public class NotifyHubServiceClient :INotifyService
    {
         
        public NotifyHubServiceClient(string customId ="", bool useRandomId=false)
        {
           

            var queryString = new Dictionary<string, string>();

            if (useRandomId)
                ConnectionId = Guid.NewGuid().ToString();

            if (!string.IsNullOrEmpty(customId))
                ConnectionId = customId;

            //to manage  specific connection id as key in Signal R connection manager
            queryString.Add("key", ConnectionId);

            // initialize Hub Connection.
            Connection = new HubConnection($"{Settings.ApiUrl}/{Settings.HubName}",queryString,useDefaultUrl:false);
            
        }



         /// <summary>
         /// Start connection purpose .
         /// </summary>
         /// <returns></returns>
        public async Task<string> ConnectAsync()
        {
            Connection.Closed += ConnectionClosed; 
            HubProxy = Connection.CreateHubProxy($"{Settings.HubName}");

            //Handle incoming event from server: 
            HubProxy.On("Sync", (model) =>
            {
                var appointment = JsonConvert.DeserializeObject<Appointment>(model.ToString());
                if (OnNewAppointmentAdded != null) OnNewAppointmentAdded(appointment);
            });
           
            //HubProxy.On("OnDeviceAddedCallBack", (device) =>
            //{
                
            //    var d = JsonConvert.DeserializeObject<Device>(device.ToString());
            //    GetDevicesCallBack(new List<Device>() { d });
                
            //});
            //HubProxy.On("GetDevicesCallBack", (devices) => 
            //{
            //    var _devices = JsonConvert.DeserializeObject<List<Device>>(devices.Root.ToString());
            //    GetDevicesCallBack( _devices);
            //});

            //HubProxy.On("NotifyAvailableActionsCallBack", (actions) =>
            //{
            //    var _actions = JsonConvert.DeserializeObject<List<NotificationModel>>(actions.Root.ToString());
            //    GetAvailableActionsCallback(_actions);
            //});

            try
            {
                await Connection.Start();
                return "OK";
            }
            catch (Exception ex)
            {
                return "Unable to connect to server: Start server before connecting clients.";
                
                
            }
        }


        public void ConnectionClosed()
        {
             // TOdo refresh 
        }

        public void NotifyAvailableActions(string key)
        {
            throw new NotImplementedException();
        }

        public void GetDevices()
        {
          
        }

        public void GetAvailableActions(string key)
        {
            throw new NotImplementedException();
        }




        #region Private Properties
        private IHubProxy HubProxy { get; set; }

       

        private HubConnection Connection { get; set; }

         
        public string ConnectionId { get; set; } = "0001";
        public Action OnGetActions { get; set; } = null;
        public Action<Appointment> OnNewAppointmentAdded { get; set; }
        #endregion

    }
}
