﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DogMode.Models
{
    public class Company : BaseModel
    {
        [Display(Name = "Nombre")]
        public string Name { get; set; }


        [Display(Name = "Ubicacion")]
        public string Location { get; set; }

        [Display(Name = "Descripcion")]
        public string Description { get; set; }

        [Display(Name = "Administrador")]
        public string Administrator { get; set; }

        [Display(Name = "Contacto")]
        public string Personcontact { get; set; }

        //public List<ApplicationUser> Publishers { get; set; } = new List<ApplicationUser>();
    }
}
