﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DogMode.Models
{
    public class Category : BaseModel
    {
        public Category()
        {
            CreatedDate = DateTime.Now;
        }

        [Display(Name = "Nombre")]
        public string Name { get; set; }

    }
}
