﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DogMode.Models
{
    public class UserProfile : BaseModel
    {
        public string UserId { get; set; }

        //public User User { get; set; } 

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Claims { get; set; }
    }
}
