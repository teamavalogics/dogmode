﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DogMode.Models
{
    public class DogOwner : BaseModel
    {
        public DogOwner()
        {
            CreatedDate = DateTime.Now;
        }

        [Required]

        [Display(Name = "First Name")]
        public string Name { get; set; }

        [Display(Name = "Last Name")]

        public string LastName { get; set; }

        public List<Dog> Dogs { get; set; } = new List<Dog>();
    }
}
