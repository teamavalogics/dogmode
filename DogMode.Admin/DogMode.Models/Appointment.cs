﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DogMode.Models
{
    public class Appointment : BaseModel
    {
        public Appointment()
        {
            CheckIn = DateTime.Now;
            CheckOut = DateTime.Now;
        }


        public string OwnerSearchName { get; set; }
        public Dog Dog { get; set; }

        [Display(Name = "Dog")]
        public int DogId { get; set; }

        [Display(Name = "Check In")]
        public DateTime CheckIn { get; set; }

        [Display(Name = "Check Out")]
        public DateTime CheckOut { get; set; }

        [Display(Name = "Paid Hours")]
        public decimal PaidHours { get; set; }

        public AppointmentType Type { get; set; }

        public QueueStatus Status { get; set; }
    }
}
