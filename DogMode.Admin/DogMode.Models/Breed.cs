﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DogMode.Models
{
    public class Breed : BaseModel
    {
        [Required]
        public string Name { get; set; }
        public List<Dog> Dogs { get; set; } = new List<Dog>();
    }
}
