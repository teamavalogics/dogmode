﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DogMode.Models
{
    public partial class Dog : BaseModel
    {
        public Dog()
        {
            LastUpdate = DateTime.Now;
            CreatedDate = DateTime.Now;
        }

        [Required]
        [Display(Name = "First Name")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        public Breed DogBreed { get; set; }


        public string DogBreeadForSearch { get; set; }

        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Breed Is Required")]
        [Display(Name = "Dog Breed")]
        public int DogBreedId { get; set; }


        [Display(Name = "Status")]
        public DogStatus Status { get; set; }

        [Display(Name = "Last Update")]
        public DateTime LastUpdate { get; set; }

        [Display(Name = "Dog Picture")]
        public string Image { get; set; }


        [Display(Name = "Icono")]
        public string Icon { get; set; }

        public string DogOwnerForSearch { get; set; }
        public DogOwner DogOwner { get; set; }
        [Required]
        [Display(Name = "Owner")]
        public int DogOwnerId { get; set; }
        public List<Appointment> Appointments { get; set; } = new List<Appointment>();


    }
}
