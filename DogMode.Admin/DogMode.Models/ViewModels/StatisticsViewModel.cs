﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DogMode.Models
{
    public class StatisticsViewModel
    {
        public int FeedId { get; set; }

        public bool HasValorations { get; set; }

        public int ViewCounts { get; set; }

        public int ValorationsCount { get; set; }

        public string Title { get; set; }

        public string Author { get; set; }

        public string AuthorName { get; set; }

        public string Email { get; set; }

        public DateTime DateStart { get; set; }

        public DateTime DateEnd { get; set; }

        public Category Category { get; set; }

        public int CategoryId { get; set; }


        // Options selection 

        public int OptionCount1 { get; set; }

        public string OptionLabel1 { get; set; }

        public int OptionCount2 { get; set; }

        public string OptionLabel2 { get; set; }
        public int OptionCount3 { get; set; }

        public string OptionLabel3 { get; set; }
        public int OptionCount4 { get; set; }

        public string OptionLabel4 { get; set; }
        public int OptionCount5 { get; set; }

        public string OptionLabel5 { get; set; }

    }
}
