﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DogMode.Models
{
    public class AppointmentViewModel
    {
        public int Id { get; set; }

        public DateTime CheckIn { get; set; }

        public DateTime Checkout { get; set; }

        public DateTime CreatedDate { get; set; }

        public int DogId { get; set; }

        public string DogName { get; set; }

        public string DogBreed { get; set; }

        public int DogOwnerId { get; set; }

        public string OwnerFirstName { get; set; }

        public string OwnerLastName { get; set; }

        public decimal PaydHours { get; set; }

        public AppointmentType Type { get; set; }

        public QueueStatus Status { get; set; }
        public string Image { get; set; }
        public string DogLastName { get; set; }
    }
}
