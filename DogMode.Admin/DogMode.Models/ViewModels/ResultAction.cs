﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DogMode.Models
{
    public class ResultAction
    {
        public bool success { get; set; } = false;

        public string message { get; set; } = string.Empty;
    }
}
