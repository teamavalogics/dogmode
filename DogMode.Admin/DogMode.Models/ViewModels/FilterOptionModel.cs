﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DogMode.Models
{
    public class FilterOptionModel
    {
        public FilterOptionModel()
        {

        }
        public FilterOptionModel(string companyIdClaim)
        {

            companyId = !string.IsNullOrEmpty(companyIdClaim) ? Convert.ToInt32(companyIdClaim) : 0;
        }
        public FilterOptionModel(int? companyId)
        {
            this.companyId = companyId;
        }
        public string keywords { get; set; }

        public DateTime? date { get; set; }

        public string time { get; set; }

        public DateTime? end { get; set; }

        public string lat { get; set; }

        public string lon { get; set; }


        public int page { get; set; } = 1;

        public int Limit { get; set; } = 35;

        public bool IsDesc { get; set; }

        public string OrderByProperty { get; set; }

        public string role { get; set; }

        public int? companyId { get; set; }

        public int Skip
        {
            get { return page == 1 || page == 0 ? 0 : (page - 1) * Limit; }
        }

        public bool HasOrderByProperty
        {
            get { return !string.IsNullOrEmpty(OrderByProperty); }
        }

        public List<string> SearchKeys
        {
            get
            {
                return (keywords ?? "").ToLower().Trim().Split(' ')
                    .Where(key => !string.IsNullOrEmpty(key)).ToList();
            }
        }
        
        public int CategoryId { get; set; }
        public DogStatus? Status { get; set; }

        /// <summary>
        /// User Id to filter by
        /// </summary>
        public string UserId { get; set; }


        public AppointmentType? type { get; set; }

        public QueueStatus? QueueStatus { get; set; }

        public bool TodatyCheckoutAppointments { get; set; }
    }
}
