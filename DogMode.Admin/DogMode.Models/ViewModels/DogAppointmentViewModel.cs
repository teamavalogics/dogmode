﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DogMode.Models
{
    public class DogAppointmentViewModel
    {
        public int Id { get; set; }

        [Display(Name = "Name")]
        public string Name { get; set; }

        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        public string DogBreeadForSearch { get; set; }

        [Required]
        [Display(Name = "Dog Breed")]
        public int DogBreedId { get; set; }


        [Display(Name = "Status")]
        public DogStatus Status { get; set; }

        public DateTime LastUpdate { get; set; }

        [Display(Name = "Dog Picture")]
        public string Image { get; set; }

        [Display(Name = "Icono")]
        public string Icon { get; set; }

        public string DogOwnerForSearch { get; set; }

        [Required]
        [Display(Name = "Owner")]
        public int DogOwnerId { get; set; }

        public decimal PaidHours { get; set; }

        public DateTime CheckIn { get; set; } = DateTime.Now;

        public DateTime CheckOut { get; set; } = DateTime.Now;

        public AppointmentType Type { get; set; }
        public string CheckInDate { get; set; }
        public string CheckOutDate { get; set; }
    }
}
