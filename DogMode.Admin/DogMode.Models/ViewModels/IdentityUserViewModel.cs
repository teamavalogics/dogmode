﻿using System.ComponentModel.DataAnnotations;

namespace DogMode.Models
{
    public class IdentityUserViewModel
    {
        public string Id { get; set; }

        public string Email { get; set; }

        [Display(Name = "Phone 2")]
        public string Phone_2 { get; set; }

        public int AccessFailedCount { get; set; }

        public string Role { get; set; }

        public string Password { get; set; }

        [Display(Name = "Force Change Password")]
        public bool ForceChangePassword { get; set; }

        [Display(Name = "First Name")]
        public string FirstName { get; set; }


        [Display(Name = "Last Name")]
        public string LastName { get; set; }


        //[StringLength(20)]
        //public string PHONE_2 { get; set; }

        [StringLength(200)]
        [Display(Name = "Address")]
        public string Address { get; set; }

        //[Required]
        [Display(Name = "UserName")]
        public string UserName { get; set; }

        [Display(Name = "Profile image")]
        public string ProfilePicture { get; set; }

        public string ProfilePictureDefault
        {
            get { return !string.IsNullOrEmpty(ProfilePicture) ? ProfilePicture : "/Images/profile.jpg"; }
        }
        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }
        public bool DisableForCenter { get; set; }


        [Display(Name = "Company")]
        public string CompanyNameForAutoComplete { get; set; }

        [Display(Name = "Company")]
        public int CompanyId { get; set; }

        //public List<IsInRole> OnRoles { get; set; } 
    }
}
