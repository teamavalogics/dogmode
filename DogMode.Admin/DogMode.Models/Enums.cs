﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DogMode.Models
{
    public enum DogStatus
    {
        Pending = 0,
        Active = 1,
        Inactive = 2,
    }

    public enum CampaignStatus
    {
        Active,
        Inactive,
    }

    public enum EventTypes
    {
        Open = 0,
    }
    
    public enum DaysOfWeek
    {
        Mon,
        Tue,
        Wed,
        Thu,
        Fri,
        Sat,
        Sun,
    }

    public enum AppointmentType
    {

        OpenSwim = 0,

        KennelDayCare = 1,
    }

    public enum QueueStatus
    {
        Pending = 0,
        Active = 1,
        Completed = 2,
    }

}
