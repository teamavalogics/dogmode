﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Extensions
{
    public static class Extensions
    {
        //public static string ToAzureRelativePath(this HttpPostedFileBase file, int productId)
        //{
        //    var fileName = System.IO.Path.Combine(productId.ToString(CultureInfo.InvariantCulture),
        //                System.IO.Path.GetFileName(file.FileName).ToLower());
        //    return fileName;
        //}

        //public static string ToAzureRelativePath(this string fullPath, int productId)
        //{
        //    var fileName = System.IO.Path.Combine(productId.ToString(CultureInfo.InvariantCulture),
        //                System.IO.Path.GetFileName(fullPath).ToLower());
        //    return fileName;
        //}

        public static byte[] ToByteArray(this Stream stream)
        {
            using (BinaryReader br = new BinaryReader(stream))
            {
                return br.ReadBytes((int)stream.Length);
            }
        }

        public static MemoryStream GetStreamFromFile(string path)
        {
            var bytes = System.IO.File.ReadAllBytes(path);

            System.IO.File.Delete(path);
            return new MemoryStream(bytes);

        }
    }
}
