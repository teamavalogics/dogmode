﻿using DogMode.BusinessAccess.Extensions;
using DogMode.Context;
using DogMode.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DogMode.BusinessAccess.Repositories
{
    public class StatisticsRepository : BaseRepository<ApplicationContext, Dog>
    {


        

        

        public IQueryable<StatisticsViewModel> Search(FilterOptionModel filter)
        {

            IQueryable<Dog> query = Context.Dogs;

            filter.SearchKeys.ForEach(keyword => query = query.Where(f => f.Name.ToLower().Contains(keyword) 
                                                         //f.AuthorName.ToLower().Contains(keyword))
                                                         //f.Author.ToLower().Contains(keyword) ||
           
                                                         ));
            // TODO implement date range filters

            //if (filter.CategoryId > 0) query = query.Where(c => c.CategoryId == filter.CategoryId);

            if (filter.Status.HasValue) query = query.Where(f => f.Status == filter.Status.Value);




            return query.ToStatisticsViewModel();

        }

        public Task<List<StatisticsViewModel>> GetMostViewedFeeds()
        {
            IQueryable<Dog> query = Context.Dogs;

            return query.ToStatisticsViewModel()
                .OrderByDescending(i => i.ViewCounts).Take(5).ToListAsync();

        }


        public Task<List<int>> ViewsLastSixMonthsAsync()
        {
            var referenceDate = DateTime.Now.AddMonths(-5);


            var fromDate = new DateTime(referenceDate.Year, referenceDate.Month, 1);

            return Context.Dogs.ToStatisticsViewModel()
                .Where(f => f.DateStart >= fromDate && f.ViewCounts >= 0)
                .GroupBy(f => f.DateStart.Month)
                .Select(f => f.Count()).ToListAsync();
        }



        public async Task<List<int>> ViewsLasMonthAsync()
        {
            var referenceDate = DateTime.Now;


            var fromDate = new DateTime(referenceDate.Year, referenceDate.Month, 1);

            var model = await Context.Dogs.ToStatisticsViewModel()
                .Where(f => f.DateStart >= fromDate && f.ViewCounts > 0)
                .GroupBy(f =>
                   f.DateStart.Day >= 1 && f.DateStart.Day <= 7 ? 1 :
                    f.DateStart.Day >= 8 && f.DateStart.Day <= 15 ? 2 :
                    f.DateStart.Day >= 16 && f.DateStart.Day <= 23 ? 3 :
                    f.DateStart.Day >= 24 && f.DateStart.Day <= 30 ? 4 : 0
                )
                .Select(f => new { week = f.Key, count = f.Count() })
                .ToDictionaryAsync(k => k.week.ToString(), val => val.count);

            if (!model.ContainsKey("1")) model.Add("1", 0);
            if (!model.ContainsKey("2")) model.Add("2", 0);
            if (!model.ContainsKey("3")) model.Add("3", 0);
            if (!model.ContainsKey("4")) model.Add("4", 0);

            return model.Select(val => val.Value).ToList();


        }
    }
}
