﻿using DogMode.BusinessAccess.Extensions;
using DogMode.Context;
using DogMode.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace DogMode.BusinessAccess.Repositories
{
    public class FeedsRepository : BaseRepository<ApplicationContext, Dog>
    {


        private IQueryable<Dog> CommonSearch(FilterOptionModel filter, Guid? user,string facebookUserId ="")
        {
            IQueryable<Dog> query = GetSummary(user);
            //.Include(f => f.User);

            filter.SearchKeys.ForEach(k => query =
            query.Where(q => q.Name.ToLower().Contains(k)
                           || q.LastName.ToLower().Contains(k)
                           
                           ));

            //if (!string.IsNullOrEmpty(filter.CategoryName)) query = query.Where(q => q.Category.Name.ToLower().Contains(filter.CategoryName.Trim().ToLower()));

           
            if (filter.Status.HasValue) query = query.Where(b => b.Status == filter.Status.Value);
            else query = query.Where(f => f.Status == DogStatus.Active);

            ////add filter by  company
            //if (filter.CompanyToFilterId != 0 && !string.IsNullOrEmpty(filter.CompanyName))
            //   // query = query.Where(f => f.Campaing.CompanyId == filter.CompanyToFilterId);

            //// add filter by Campaign
            //if (filter.CampaignId != 0 && !string.IsNullOrEmpty(filter.CampaignName))
            //   /// query = query.Where(f => f.Campaing.Id == filter.CampaignId);

            //// filter by beacon by active status by default.
            //if (!string.IsNullOrEmpty(filter.BeaconId) && filter.Mayor != 0 && filter.Menor != 0)
            //{

            //    var beaconId = new Guid(filter.BeaconId);
            //    //query = query.Where(f => f.Beacon.BeaconId == beaconId && f.Beacon.Mayor == filter.Mayor && f.Beacon.Menor == filter.Menor );

            //}
            ////filter by beacon int id.
            //if (!string.IsNullOrEmpty(filter.BeaconId) &&
            //    filter.BeaconId.Where(c=> Char.IsDigit(c)).Count()== filter.BeaconId.Length )
            //{
            //    var beaconId = Convert.ToInt32(filter.BeaconId);
            //    //query = query.Where(f => f.BeaconId== beaconId);
            //}
            ////filter ads by user and viewed previously
            //if (filter.ViewedByUser && !string.IsNullOrEmpty(filter.UserId))
            //{
            //    //query = (from feed in query
            //    //         join feeedEvent in Context.FeedEvents.Where(e => e.ModifiedBy == filter.UserId )
            //    //         on feed.Id equals feeedEvent.FeedId
            //    //         select feed).Distinct();
            //}


            return filter.HasOrderByProperty ? query.CustomOrderby(filter) : query.OrderByDescending(o => o.LastUpdate);
        }

        

        public Task<bool> HasActiveDogs()
        {
            return Context.Dogs.AnyAsync();
        }

        public IQueryable<Dog> GetSummary(Guid? userid = null,string facebookUserId ="")
        {

            // add access visibility  level
            IQueryable<Dog> query = Context.Dogs.Include(f => f.DogOwner);


            //by viewed feeds per user
            if(!string.IsNullOrEmpty(facebookUserId))
            {
                //query = (from feed in query
                //         join feedEvent in Context.FeedEvents.Where(fe => fe.ModifiedBy == facebookUserId)
                //         on feed.Id equals feedEvent.FeedId into defaultEvents
                //         from FeedEvent in defaultEvents.DefaultIfEmpty()
                //         where FeedEvent == null
                //         select feed);
            }

            ////filter by access level
            var centerId = ClaimsPrincipal.Current.CenterId();

            //if (centerId.HasValue)
            //{
            //    query = query.Where(q => q.User.CompanyId == centerId);
            //} 
            return query;
        }

       


        public IQueryable<Dog> GetSummary(FilterOptionModel filter)
        {
            return CommonSearch(filter, UserId)
                .OrderByDescending(o => o.LastUpdate);
        }


        public Task<List<Dog>> GetSummaryAsync(FilterOptionModel filter)
        {
            return CommonSearch(filter, UserId)
                .OrderByDescending(o => o.LastUpdate)
                .Skip(filter.Skip).Take(filter.Limit).ToListAsync();
        }

 

        //public dynamic MessageForStatus(FeedStatus status)
        //{
        //    switch (status)
        //    {
        //        case FeedStatus.Pending: return "Do you Want set To pending?";
        //        case FeedStatus.Active: return "Do you want Activate ?";
        //        case FeedStatus.Inactive: return "Do you want Deactivate?";
        //        default: return string.Empty;
        //    }
        //}

        public async Task<int> GetPageLimit(FilterOptionModel filter)
        {
            return (await CommonSearch(filter, UserId).CountAsync()) / filter.Limit + 1;
        }

        public Task GetUserSummaryAsync(Guid id)
        {
            throw new NotImplementedException();
        }

        public override void InsertOrUpdate(Dog model)
        {
           // if (model.Id == 0) model.IdPublisher = ShouldBeUserId;
            // to avoid corrupted data just check the refences. and save changes
            //model.Beacon = null;
            //model.Campaing = null;

            base.InsertOrUpdate(model);
            
        }


    }
}
