﻿using DogMode.Context;
using DogMode.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Threading.Tasks;
using DogMode.BusinessAccess.Extensions;

namespace DogMode.BusinessAccess.Repositories
{
    public class DogOwnerRepository :BaseRepository<ApplicationContext,DogOwner>
    {

        private IQueryable<DogOwner> CommonSearch(FilterOptionModel filter, Guid? user)
        {
            IQueryable<DogOwner> query = GetSummary(user);
            //.Include(f => f.User);

            filter.SearchKeys.ForEach(k => query =
            query.Where(q => q.Name.ToLower().Contains(k)
                           || q.LastName.ToLower().Contains(k)

                           ));
             // TODO add filter logic.



            return filter.HasOrderByProperty ? query.CustomOrderby(filter) : query.OrderByDescending(o => o.Id);
        }

        public IQueryable<DogOwner> GetSummary(Guid? userid = null)
        {

            // add access visibility  level
            IQueryable<DogOwner> query = Context.DogOwners.Include(o=> o.Dogs);

             
            //if (centerId.HasValue)
            //{
            //    query = query.Where(q => q.User.CompanyId == centerId);
            //} 
            return query;
        }

        public async Task<List<DogOwner>> SearchForAutoCompleteAsync(string query)
        {
            IQueryable<DogOwner> q = GetSummary();

             q = CommonSearch(new FilterOptionModel() { keywords = query },null);

            var owners= await q.Take(20).ToListAsync();

            return owners.Select(o => new DogOwner() { Id = o.Id, Name = o.Name +" "+ o.LastName }).ToList();
        }

        public IQueryable<DogOwner> GetSummary(FilterOptionModel filter)
        {
            return CommonSearch(filter, UserId)
                .OrderByDescending(o => o.Id);
        }


        public Task<List<DogOwner>> GetSummaryAsync(FilterOptionModel filter)
        {
            return CommonSearch(filter, UserId)
                .OrderByDescending(o => o.Id)
                .Skip(filter.Skip).Take(filter.Limit).ToListAsync();
        }

        
    }
}
