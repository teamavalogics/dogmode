﻿using DogMode.Context;
using DogMode.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using DogMode.BusinessAccess.Extensions;
using Newtonsoft.Json;

namespace DogMode.BusinessAccess.Repositories
{

    public class AppointmentsRepository : BaseRepository<ApplicationContext, Appointment>
    {
        private IQueryable<Appointment> CommonSearch(FilterOptionModel filter, Guid? user)
        {
            IQueryable<Appointment> query = GetSummary(user);
            //.Include(f => f.User);

            filter.SearchKeys.ForEach(k => query =
            query.Where(q => q.Dog.Name.ToLower().Contains(k)
                           || q.Dog.LastName.ToLower().Contains(k)

                           ));
            // TODO add filter logic.
            if (filter.date.HasValue)
                query = query.Where(q => q.CheckIn >= filter.date.Value);

            if (filter.end.HasValue && !filter.date.HasValue)
            {
                var date = DateTime.Now.Date;
                query = query.Where(q => q.CheckOut >= date && q.CheckOut <= filter.end);
            }
            else if (filter.end.HasValue)
                query = query.Where(q => q.CheckOut <= filter.end.Value);

            if (filter.type.HasValue)
                query = query.Where(q => q.Type == filter.type.Value);

            if (filter.QueueStatus.HasValue)
                query = query.Where(q => q.Status == filter.QueueStatus.Value);

            return filter.HasOrderByProperty ? query.CustomOrderby(filter) : query.OrderByDescending(o => o.CheckIn);
        }

      

        public IQueryable<Appointment> GetSummary(Guid? userid = null)
        {

            // add access visibility  level
            IQueryable<Appointment> query = Context.Appointments.Include(i => i.Dog)
                .Include(i => i.Dog.DogOwner)
                .Include(i => i.Dog.DogBreed);


            //if (centerId.HasValue)
            //{
            //    query = query.Where(q => q.User.CompanyId == centerId);
            //} 
            return query;
        }

        public Task<Dog> FindDogAsync(int dogId)
        {
            return Context.Dogs.Include(d => d.DogBreed).FirstAsync(d => d.Id == dogId);
        }

        public Task<List<DogAppointmentViewModel>> GetDogsAppointmentViewModelAsync(int ownerId)
        {
            return Context.Dogs.Where(d => d.DogOwnerId == ownerId)
                .Select(d => new DogAppointmentViewModel()
                {
                    Id = d.Id,
                    Name = d.Name,
                    LastName = d.LastName,
                    DogBreedId = d.DogBreedId,
                    Status = d.Status,
                    LastUpdate = d.LastUpdate,
                    Image = d.Image,
                    Icon = d.Icon,
                    DogBreeadForSearch = d.DogBreed.Name,
                    DogOwnerId = d.DogOwnerId,
                    PaidHours = 0,
                    CheckIn = DateTime.Now,
                    CheckOut = DateTime.Now,
                })
                .ToListAsync();
        }



        public async Task<bool> UpdateStatusAsync(int id, QueueStatus status)
        {
            var model = await base.FindByIdAsync(id);

            model.Status = status;

            // validation for limit 10 to pool area
            if (status == QueueStatus.Active &&
                model.Type == AppointmentType.OpenSwim)
            {
                var isvalid = (await Context.Appointments.Where(a => a.Type == AppointmentType.OpenSwim && a.Status == QueueStatus.Active)
                    .CountAsync()) < 10;

                if (isvalid)
                {
                    await SaveAsync();
                    return true;
                }
                else return false;
            }

            await SaveAsync();

            return true;
        }


        /// <summary>
        /// create appointmentes from list. 
        /// </summary>
        /// <param name="appointments"></param>
        /// <returns></returns>
        public async Task<ResultAction> CreateAppointments(List<DogAppointmentViewModel> appointments)
        {
            var appointmentsToAdd = appointments
                .Where(a => a.CheckOut >= a.CheckIn).ToAppointments(); 

            var result = new ResultAction();
            result.success= await Context.Appointments.IsValidLimitForOpenSwimAsync(appointments);

            if (!result.success)
            {
                result.message = "Open Swim Limit has been exceeded";
                result.success = false;
                return result;
            }

            if (appointments.Any(a => ((DateTime.Now - a.CheckOut).Hours >= 1)))
            {
                result.message=  "Please Select a valid check in time";
                result.success = false;
                return result;
            }
            // validate scheduled intersection intervals
            var dogsIds = appointments.Select(d => d.Id).ToList();

            // TODO improve performance for that query.
            appointments.ForEach(a =>
            {
                var duplicated = Context.Appointments.Any(ap =>ap.DogId == a.Id &&
                 (a.CheckIn >= ap.CheckIn && a.CheckIn < ap.CheckOut && ap.Status == QueueStatus.Active) 
                 
                );

                if (duplicated)
                {
                    result.success = false;
                    result.message += $"Please update current queue entry for {a.Name} {a.LastName}\n";
                }
            });

            if (!result.success) return result;

            appointmentsToAdd.ForEach(a => Context.Entry(a).State = EntityState.Added);

            await Context.SaveChangesAsync();

            return  result ;
        }

        public IQueryable<Appointment> GetSummary(FilterOptionModel filter)
        {
            return CommonSearch(filter, UserId)
                .OrderByDescending(o => o.Id);
        }


        public Task<List<Appointment>> GetSummaryAsync(FilterOptionModel filter)
        {
            return CommonSearch(filter, UserId)
                .OrderByDescending(o => o.Id)
                .Skip(filter.Skip).Take(filter.Limit).ToListAsync();
        }

        public Task<List<AppointmentViewModel>> SearchAsync(FilterOptionModel filter)
        {
            IQueryable<Appointment> query = CommonSearch(filter, UserId);

            // to display only active appointments for client app.
            if (!filter.QueueStatus.HasValue) query = query.Where(a => a.Status == QueueStatus.Active);

            return query.OrderBy(a => a.CheckOut)
               .ToAppointmentViewModel()
               .Skip(filter.Skip)
               .Take(filter.Limit)
               .ToListAsync();
        }


        public Task<Appointment> GetDetailedAppointmentAsync(int id)
        {
            return Context.Appointments
                .Include(i => i.Dog)
                .Include(i => i.Dog.DogOwner)
                .Include(i => i.Dog.DogBreed)
                .FirstOrDefaultAsync(a => a.Id == id);
        }

        public async Task<ResultAction> CreateNewDogAsync(Dog model)
        {
            var result = new ResultAction() { success = false } ;
            try
            {

                if (string.IsNullOrEmpty(model.Name) && string.IsNullOrEmpty(model.LastName))
                    return result;


                // Check if exist owner name based on lastname of dog. 
                var existsOwnerName = await Context.DogOwners.AnyAsync(o => o.Name == model.DogOwner.Name && o.LastName == model.LastName);

                if (existsOwnerName)
                {
                    result.message =  "Owner First Name and Last Name Already Exist";
                    return result;
                }


                if (string.IsNullOrEmpty(model.DogOwner.Name))
                {
                    model.DogOwner = null;
                }
                else
                {
                    model.DogOwner.LastName = model.LastName;

                    Context.Entry(model.DogOwner).State =  EntityState.Added  ;
                }
                Context.Entry(model).State = EntityState.Added;

                await Context.SaveChangesAsync();

                result.success = true;
                return result;
            }
            catch (Exception ex)
            {
                result.message = ex.Message;
                result.success = false;
                return result;
            }
        }



        public Task<List<AppointmentViewModel>> SearchAsync()
        {
            var date = DateTime.Now.Date;
            var filter = new FilterOptionModel()
            {
                date = null,
                end = date.AddDays(1),
                TodatyCheckoutAppointments = true,
            };

            return SearchAsync(filter);
        }

        public Task<List<AppointmentViewModel>> SearchAsync(DateTime date)
        {
            var filter = new FilterOptionModel()
            {
                date = null,
                end = date.AddDays(1),
                TodatyCheckoutAppointments = true,
            };

            return SearchAsync(filter);
        }

        public Task<List<AppointmentViewModel>> SearchAsync(AppointmentType type)
        {
            var date = DateTime.Now.Date;
            var filter = new FilterOptionModel()
            {
                date = null,
                end = date.AddDays(1),
                type = type,
                TodatyCheckoutAppointments = true,
            };

            return SearchAsync(filter);
        }

        public Task<bool> HasAppointmentsAsync()
        {
            return Context.Appointments.AnyAsync();
        }

        public Task<AppointmentViewModel> FindAppointmentViewModel(int id)
        {
            return GetSummary().Where(a => a.Id == id).ToAppointmentViewModel().FirstOrDefaultAsync();
        }


        public Task<AppointmentViewModel> GetAppointmentViewModelFromDogId(int id)
        {
            return Context.Dogs.Where(d => d.Id == id)
            .ToAppointmentViewModel().FirstOrDefaultAsync();
        }

        public Task< string >GetOwnerNameAsync(int ownerId)
        {
            return Context.DogOwners.Where(o => o.Id == ownerId).Select(o => o.Name + " " + o.LastName).FirstOrDefaultAsync();
        }

        public Task<decimal> GetPaidHoursAsync(int id)
        {
            return Context.Appointments.Where(a => a.Id == id).Select(a => a.PaidHours).FirstOrDefaultAsync();
        }
    }
}
