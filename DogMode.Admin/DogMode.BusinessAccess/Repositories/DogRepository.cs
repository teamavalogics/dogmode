﻿using DogMode.BusinessAccess.Extensions;
using DogMode.Context;
using DogMode.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Threading.Tasks;

namespace DogMode.BusinessAccess.Repositories
{
    public class DogRepository : BaseRepository<ApplicationContext, Dog>
    {

        private IQueryable<Dog> CommonSearch(FilterOptionModel filter, Guid? user)
        {
            IQueryable<Dog> query = GetSummary(user);
            //.Include(f => f.User);

            filter.SearchKeys.ForEach(k => query =
            query.Where(q => q.Name.ToLower().Contains(k)
                           || q.LastName.ToLower().Contains(k)

                           ));
            // TODO add filter logic.



            return filter.HasOrderByProperty ? query.CustomOrderby(filter) : query.OrderByDescending(o => o.Id);
        }

        public IQueryable<Dog> GetSummary(Guid? userid = null)
        {

            // add access visibility  level
            IQueryable<Dog> query = Context.Dogs.Include(o => o.DogOwner);


            //if (centerId.HasValue)
            //{
            //    query = query.Where(q => q.User.CompanyId == centerId);
            //} 
            return query;
        }




        public IQueryable<Dog> GetSummary(FilterOptionModel filter)
        {
            return CommonSearch(filter, UserId)
                .Include(d => d.DogBreed)
                .Include(d => d.DogOwner)
                .OrderByDescending(o => o.Id);
        }


        public Task<List<Dog>> GetSummaryAsync(FilterOptionModel filter)
        {
            return CommonSearch(filter, UserId)
                .OrderByDescending(o => o.Id)
                .Skip(filter.Skip).Take(filter.Limit).ToListAsync();
        }

        public Task<List<Dog>> SearchForAutoComplete(string keys)
        {
            var keywords = (keys ?? "").ToLower().Trim(' ').ToList();

            IQueryable<Dog> query = Context.Dogs.OrderBy(o => o.Name);

            keywords.ForEach(k => query = query.Where(d => d.Name.ToLower().Contains(k) || d.LastName.ToLower().Contains(k)));

            return query.Take(20).Select(d=> new Dog()
            {
                Id= d.Id,
                Name = d.Name +" " +d.LastName

            }).ToListAsync();
        }

        public override void InsertOrUpdate(Dog model)
        {
            if (model.DogBreed!=null && model.DogBreed.Id == 0)
                Context.Entry(model.DogBreed).State = EntityState.Detached;
            if (model.DogOwner!=null&& model.DogOwner.Id == 0)
                Context.Entry(model.DogOwner).State = EntityState.Detached;

            model.LastUpdate = DateTime.Now;

            base.InsertOrUpdate(model);
        }

    }
}
