﻿using DogMode.Context;
using DogMode.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DogMode.BusinessAccess.Repositories
{
    public class FeedValorationRepository : BaseRepository<ApplicationContext, FeedValoration>
    {


        public virtual async Task<FeedValoration> Create(FeedValoration valoration)
        {

            Context.FeedValorations.Add(valoration);

            await Context.SaveChangesAsync();

            return valoration;
        }

        public async Task<bool> OnfeedOpen(int feedId,string facebookUserId)
        {

            Context.FeedEvents.Add(new FeedEvent()
            {
                FeedId = feedId,
                Type = EventTypes.Open,
                ModifiedBy = facebookUserId,
            });

            await SaveAsync();

            return true;
        }
    }
}
