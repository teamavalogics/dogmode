﻿using DogMode.Context;
using DogMode.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DogMode.BusinessAccess.Repositories
{
    public class BreedsRepository :BaseRepository<ApplicationContext,Breed>
    {

        public Task  <List<Breed>> GetAllAsync()
        {
            return Context.Breeds.ToListAsync();
        }

        public Task< List<Breed>> SearchAsync(string query)
        {
            var keyworkds = ((query) ?? "").ToLower().Split(' ').ToList() ;


            IQueryable<Breed> q = Context.Breeds.OrderBy(b => b.Name);

            // simple search
            keyworkds.ForEach(k =>  q= q.Where(b => b.Name.ToLower().Contains(k)));

            return q.Take(20).ToListAsync();
        }
    }
}
