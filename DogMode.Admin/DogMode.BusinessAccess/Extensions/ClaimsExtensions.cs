﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace DogMode.BusinessAccess.Extensions
{
    public static class ClaimsExtensions
    {
        public static string GetFacebookId(this ClaimsPrincipal claims)
        {
            var claimId = claims.Claims.FirstOrDefault(c => c.Type.EndsWith("nameidentifier"));

            return claimId.Value ?? string.Empty;
        }

        public static string GetFacebookId(this IEnumerable<Claim> claims)
        {
            return claims.Any(c => c.Type.EndsWith("nameidentifier")) ?
                  claims.Where(c => c.Type.EndsWith("nameidentifier")).Select(c => c.Value)
                  .FirstOrDefault() : string.Empty;
        }

        public static string FindFirst(this IEnumerable<Claim> claims, string key)
        {
            var claim = claims.FirstOrDefault(c => c.Type == key);

            return claim != null ? claim.Value : string.Empty;
        }
    }
}
