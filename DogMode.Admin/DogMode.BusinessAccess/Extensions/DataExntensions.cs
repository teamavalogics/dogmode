﻿using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace DogMode.BusinessAccess.Extensions
{
    public static class DataExtensions
    {

        public static List<string> ToKeyWords(this string value)
        {
            return (value ?? "").ToLower().Split(' ').ToList();
        }



        public static Tmodel Deserialize<Tmodel>(string value) where Tmodel : class, new()
        {
            if (string.IsNullOrEmpty(value)) return new Tmodel();

            return JsonConvert.DeserializeObject<Tmodel>(value);
        }

        public static string Serialize<Tmodel>(this Tmodel model)
        {
            return JsonConvert.SerializeObject(model);
        }

        public static void Assign(this object destination, object source)
        {
            if (destination is IEnumerable && source is IEnumerable)
            {
                var dest_enumerator = (destination as IEnumerable).GetEnumerator();
                var src_enumerator = (source as IEnumerable).GetEnumerator();
                while (dest_enumerator.MoveNext() && src_enumerator.MoveNext())
                    dest_enumerator.Current.Assign(src_enumerator.Current);
            }
            else
            {
                var destProperties = destination.GetType().GetProperties();
                foreach (var sourceProperty in source.GetType().GetProperties())
                {
                    foreach (var destProperty in destProperties)
                    {
                        if (destProperty.Name == sourceProperty.Name &&
                            destProperty.PropertyType.IsAssignableFrom(sourceProperty.PropertyType) && destProperty.CanWrite)
                        {
                            destProperty.SetValue(destination, sourceProperty.GetValue(source, new object[] { }),
                                new object[] { });
                            break;
                        }
                    }
                }
            }
        }
    }
}
