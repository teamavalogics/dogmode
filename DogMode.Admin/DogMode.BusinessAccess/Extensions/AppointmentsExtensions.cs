﻿using DogMode.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DogMode.BusinessAccess.Extensions
{
    public static class AppointmentsExtensions
    {

        public static IQueryable<AppointmentViewModel> ToAppointmentViewModel(this IQueryable<Appointment> query)
        {
            return query.Select(a => new AppointmentViewModel()
            {
                Id = a.Id,
                CheckIn = a.CheckIn,
                Checkout = a.CheckOut,
                CreatedDate = a.CreatedDate,
                DogBreed = a.Dog.DogBreed.Name,
                DogId = a.DogId,
                DogOwnerId = a.Dog.DogOwnerId,
                DogName = a.Dog.Name,
                DogLastName = a.Dog.LastName,
                OwnerFirstName = a.Dog.DogOwner.Name,
                OwnerLastName = a.Dog.DogOwner.LastName,
                Type = a.Type,
                PaydHours = a.PaidHours,
                Image = a.Dog.Image,
                Status = a.Status,
            });
        }

        public static IQueryable<AppointmentViewModel> ToAppointmentViewModel(this IQueryable<Dog> query)
        {
            return query.Select(d => new AppointmentViewModel()
            {
                Id = 0,
                CheckIn = DateTime.Now,
                Checkout = DateTime.Now,
                CreatedDate = DateTime.Now,
                DogBreed = d.DogBreed.Name,
                DogId = d.Id,
                DogOwnerId= d.DogOwnerId,
                DogName = d.Name,
                DogLastName = d.LastName,
                OwnerFirstName= d.DogOwner.Name,
                OwnerLastName = d.DogOwner.LastName,
                Type = AppointmentType.KennelDayCare,
                PaydHours =0,
                Image = d.Image,
                Status =  QueueStatus.Pending,
            });
        }

        public static List<Appointment> ToAppointments(this IEnumerable<DogAppointmentViewModel> appointmentViewmodelList)
        {

            return appointmentViewmodelList.Select(a =>
            new Appointment()
            {
                DogId = a.Id,
                CreatedDate = DateTime.Now,
                CheckIn = a.CheckIn,
                CheckOut = a.Type == AppointmentType.OpenSwim ? a.CheckIn.AddHours((double)a.PaidHours) : a.CheckOut,
                PaidHours = a.Type == AppointmentType.KennelDayCare ? ((a.CheckOut - a.CheckIn)).Hours : a.PaidHours,
                Status = QueueStatus.Active,
                Type = a.Type,
            }).ToList();
        }


        /// <summary>
        /// Open Swim Validation Limit on create Appointment
        /// </summary>
        /// <param name="query"></param>
        /// <param name="appointments"></param>
        /// <returns></returns>
        public static async Task<bool> IsValidLimitForOpenSwimAsync(this IQueryable<Appointment> query, List<DogAppointmentViewModel> appointments)
        {
            var count = await query.CountAsync(a => a.Type == AppointmentType.OpenSwim && a.Status == QueueStatus.Active);

            return count <= AppConstants.OpenSwimLimitCount // is valid limit 
                &&  count + appointments.Count(a=> a.Type ==  AppointmentType.OpenSwim ) <= AppConstants.OpenSwimLimitCount;
        }
    }
}
