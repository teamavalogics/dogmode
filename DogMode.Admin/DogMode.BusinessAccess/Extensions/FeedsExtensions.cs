﻿using DogMode.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DogMode.BusinessAccess.Extensions
{
    public static class FeedsExtensions
    {

        /// <summary>
        /// View Model compatible For Grid Operations options Sort , Orderby yakiris makiris Etc.
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public static IQueryable<StatisticsViewModel> ToStatisticsViewModel(this IQueryable<Dog> query)
        {
            return query.OrderBy(f => f.Id).Select(f => new StatisticsViewModel()
            {
                FeedId = f.Id,
                HasValorations = false,
                //ViewCounts = f.FeedEvents.Where(ev => ev.Type == EventTypes.Open).Count(),
                //ValorationsCount = f.Valorations.Count(),
                //Title = f.Title,
                //DateStart = f.DateStart,
                //DateEnd = f.DateEnd,
                //Category = f.Category,
                //CategoryId = f.CategoryId, 
            });
        }
    }
}
