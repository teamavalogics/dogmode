﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DogMode.BusinessAccess
{
    public static class AppConstants
    {
        static AppConstants()
        {

        }

        public static string ApplicationName { get; set; } = "DogMode";

        public static string ApplicationAdministratorTitle { get; set; } = "APP ADMIN";

        public static string FeedsAdminPageTitle { get; set; } = "Administracion De Anuncios";

        public static string CompaniesPageTitle { get; set; } = "Empresas";

        public static string DateTimeFormat { get; set; }  = "MM/DD/YYYY hh:mm A";

        public static int OpenSwimLimitCount { get; set; } = 10;
    }
}
