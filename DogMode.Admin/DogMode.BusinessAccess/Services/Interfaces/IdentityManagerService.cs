﻿using DogMode.BusinessAccess.Extensions;
using DogMode.Context;
using DogMode.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace DogMode.BusinessAccess.Services
{
    public class IdentityManagerService
    {
        public IdentityManagerService(ApplicationContext context)
        {
            Context = context;
        }

        private ApplicationContext Context;

        public Task<List<IdentityUserViewModel>> GetAllUsersAsync()
        {
            return Context.Users.AsNoTracking()
                .Select(u => new IdentityUserViewModel
                {
                    UserName = u.UserName,
                    ProfilePicture = u.ProfilePicture,
                    FirstName = u.FirstName,
                    LastName = u.LastName,
                    PhoneNumber = u.PhoneNumber,

                })
                    .ToListAsync();
        }

        private IQueryable<ApplicationUser> CommonSearch(FilterOptionModel filter, ApplicationContext Context, bool onlyUsers = false)
        {
            IQueryable<ApplicationUser> users = this.Context.Users
                .Include(u => u.Roles).Include(u => u.Claims);


            if (!string.IsNullOrEmpty(filter.role))
            {

                users =
                   (from user in users
                    join userRol in Context.UserRoles on user.Id equals userRol.UserId
                    join role in Context.Roles.Where(r => r.Name == filter.role) on userRol.RoleId equals role.Id
                    select user);

            }


            filter.SearchKeys.ForEach(key =>
                         users = users.Where(u => u.UserName.ToLower().Contains(key)
                                             || u.FirstName.ToLower().Contains(key)
                                             || u.LastName.ToLower().Contains(key)
                                             //|| u.LastName.ToLower().Contains(key)
                                             //|| u.DUI.ToLower().Contains(key)
                                             || u.Email.ToLower().Contains(key)));


            return users;

        }

        /// <summary>
        ///  Common Filter 
        /// </summary>
        /// <param name="searchKeys"></param>
        /// <returns></returns>
        public async Task<List<Guid>> FilterUsers(FilterOptionModel filter, ApplicationContext Context)
        {
            var ids = await CommonSearch(filter, Context).Select(u => u.Id).ToListAsync();
            return ids.Select(i => new Guid(i)).ToList();
        }

        public Task<List<ApplicationUser>> GetAllUserNames(ApplicationContext context)
        {
            return CommonSearch(new FilterOptionModel(), context).Select(u => new ApplicationUser()
            {
                UserName = u.UserName,
                Id = u.Id
            }).ToListAsync();
        }

        public async Task<List<ApplicationUser>> GetUsersAsync(FilterOptionModel filter, ApplicationContext Context, bool onlyUsers = false)
        {

            IQueryable<ApplicationUser> users = CommonSearch(filter, Context, onlyUsers);


            var accounts = await users.Include(u => u.Claims).Include(u => u.Roles)
                .OrderBy(u => u.UserName)
                .Skip(filter.Skip).Take(filter.Limit).ToListAsync();

            //refresh profile pictures from extenal login

            foreach (var account in accounts.Where(a => a.Claims.Any()))
            {
                var claim = account.Claims.FirstOrDefault(c => c.ClaimType == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier");

                if (claim != null)
                {
                    // account.ProfilePicture = TokenExtensions.FaceBookProfilePictureFormat(claim.ClaimValue);
                }

                var claimName = account.Claims.FirstOrDefault(c => c.ClaimType == ClaimTypes.Name);
                var givenName = account.Claims.FirstOrDefault(c => c.ClaimType == ClaimTypes.GivenName);
                var phone = account.Claims.FirstOrDefault(c => c.ClaimType == ClaimTypes.MobilePhone);
                if (claimName != null && string.IsNullOrEmpty(account.FirstName)) account.FirstName = claimName.ClaimValue;
                if (givenName != null && string.IsNullOrEmpty(account.LastName)) account.LastName = givenName.ClaimValue;
                if (phone != null && string.IsNullOrEmpty(account.PhoneNumber)) account.PhoneNumber = phone.ClaimValue;

                await this.Context.SaveChangesAsync();
            }

            return accounts;
        }

        public Task<ApplicationUser> GetUserAsync(string userid)
        {
            return Context.Users.FirstOrDefaultAsync(u => u.Id == userid);
        }

        public Task<ApplicationUser> UpdateUserAsync(ApplicationUser user)
        {
            return new Task<ApplicationUser>(null);
        }

        public Task<List<string>> GetRolesAsync()
        {
            return Context.Roles.Select(r => r.Name).ToListAsync();
        }

        public async Task<bool> InsertOrUpdate(IdentityUserViewModel model, ApplicationUserManager userManager)
        {

            var user = await userManager.FindByIdAsync(model.Id);

            if (user == null)
            {
                user = new ApplicationUser();
                user.Assign(model);

                user.UserName = model.Email;
                var result = await userManager.CreateAsync(user,"1234567");
                if (!result.Succeeded) return false;
                model.Id = user.Id;
            }
            else
            {
                user .UserName =user.Email = model.Email;
                
                //user.DUI = model.DUI;
                user.PhoneNumber = model.PhoneNumber;
                user.Address = model.Address;
                //user.Category = model.Category;
                user.FirstName = model.FirstName;
                user.LastName = model.LastName;
                //user.DUI = model.DUI;
                //user.PHONE_2 = model.PHONE_2;
                user.ProfilePicture = model.ProfilePicture;
                //user.Address = model.Address;
                //user.FirstName = model.FirstName;
                //user.LastName = model.LastName;
                //user.DocumentNum = model.DocumentNum;
                //user.ProfilePicture = model.ProfilePicture;
                //if(!string.IsNullOrEmpty(model.CompanyNameForAutoComplete) && model.CompanyId>0)
                //    user.CompanyId = model.CompanyId;


                await userManager.UpdateAsync(user);
            }

            if (model.ForceChangePassword)
            {
                var tok = await userManager.GeneratePasswordResetTokenAsync(model.Id);

                var result = await userManager.ResetPasswordAsync(model.Id, tok, model.Password);

                if (!result.Succeeded) return false;
            }
            var roles = await userManager.GetRolesAsync(model.Id);

            if (!roles.Any() && !string.IsNullOrEmpty(model.Role))
            {
                var res = await userManager.AddToRoleAsync(model.Id, model.Role);
            }

            if (roles.All(r => r != model.Role) && roles.Any())
            {
                var result = await userManager.AddToRoleAsync(model.Id, model.Role);
            }
            roles.Where(r => r != model.Role).ToList().ForEach(role => userManager.RemoveFromRole(model.Id, role));

            return true;
        }


        public Task<List<ApplicationUser>> GetApplicationUsers(string keywords = "")
        {
            IQueryable<ApplicationUser> users = Context.Users.Include(i => i.Roles);


            keywords.ToLower().Split(' ').ToList()
                .ForEach(key =>
                    users = users.Where(u => u.UserName.ToLower().Contains(key)
                                        || u.Email.ToLower().Contains(key)
                                        || u.Address.ToLower().Contains(key)));

            return users.OrderBy(u => u.UserName)
                .ToListAsync();
        }

        public Task<string> GetMainRoleForUserAsync(string id)
        {
            return (from user in Context.Users.Where(u => u.Id == id)
                    join userRoles in Context.UserRoles on user.Id equals userRoles.UserId
                    join role in Context.Roles on userRoles.RoleId equals role.Id
                    select role.Name).FirstOrDefaultAsync();
        }






        /// <summary>
        /// Pagination For Grids
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public async Task<int> GetPageLimit(FilterOptionModel filter, ApplicationContext context)
        {
            return (await CommonSearch(filter, context).CountAsync()) / filter.Limit + 1;
        }



        /// <summary>
        /// Update User Claims Stored From facebook
        /// </summary>
        /// <param name="user"></param>
        /// <param name="claims"></param>
        /// <returns></returns>
        public async Task InsertOrUpdateUserClaims(string user, List<Claim> claims)
        {
            var userClaims = await Context.UserClaims.Where(c => c.UserId == user).ToListAsync();

            // update 
            foreach (var claim in claims)
            {
                var localClaim = userClaims.FirstOrDefault(c => c.ClaimType == claim.Type);
                if (localClaim == null)
                {
                    Context.UserClaims.Add(new IdentityUserClaim() { UserId = user, ClaimType = claim.Type, ClaimValue = claim.Value });
                }
                else localClaim.ClaimValue = claim.Value;
            }
            await Context.SaveChangesAsync();
        }

        public Task<List<IdentityRole>> GetRolesDataAsync()
        {
            return Context.Roles.ToListAsync();
        }

        public async Task DeleteByIdAsync(string id)
        {
            var user = await Context.Users.SingleOrDefaultAsync(u=> u.Id== id);

            Context.Entry(user).State = EntityState.Deleted;

            await Context.SaveChangesAsync();
        }
    }
}
