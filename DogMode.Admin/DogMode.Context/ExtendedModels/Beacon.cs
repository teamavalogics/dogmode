﻿using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Spatial;

namespace DogMode.Models.Extended
{
    public partial class Beacon
    {
        [Display(Name="Ubicacion del Beacon")]
        public DbGeography Location { get; set; }

        [Display(Name ="Rango de Notificaciones")]
        public int NotificationRange { get; set; }
    }
}
