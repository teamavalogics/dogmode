﻿using DogMode.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;

namespace DogMode.Context.Config
{
    public class ModelsConfigs : EntityTypeConfiguration<ApplicationUser>
    {
        public ModelsConfigs()
        {
        }
    }



    public class DogConfig : EntityTypeConfiguration<Dog>
    {
        public DogConfig()
        {

            HasRequired(f => f.DogOwner).WithMany(b => b.Dogs).HasForeignKey(f => f.DogOwnerId).WillCascadeOnDelete(false);
            HasRequired(d => d.DogBreed).WithMany(b => b.Dogs).HasForeignKey(d => d.DogBreedId).WillCascadeOnDelete(false);
            Ignore(d => d.DogBreeadForSearch);
            Ignore(d => d.DogOwnerForSearch);
        }
    }


    public class AppointmentConfig : EntityTypeConfiguration<Appointment>
    {
        public AppointmentConfig()
        {
            HasRequired(d => d.Dog).WithMany(d => d.Appointments).HasForeignKey(d => d.DogId).WillCascadeOnDelete(false);
            Ignore(a => a.OwnerSearchName);
        }
    }

    public class DogOwnerConfig : EntityTypeConfiguration<DogOwner>
    {
        public DogOwnerConfig()
        {
            Property(p => p.Name).HasColumnAnnotation("IndexUniqueName", new IndexAnnotation(new IndexAttribute() { IsUnique = true }));
            Property(p => p.LastName).HasColumnAnnotation("IndexUniqueName", new IndexAnnotation(new IndexAttribute() { IsUnique = true }));
        }
    }

}
