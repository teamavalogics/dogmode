﻿using DogMode.Context.Config;
using DogMode.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DogMode.Context
{
    public class ApplicationContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationContext() : base("DefaultConnection")
        {
            this.Configuration.LazyLoadingEnabled = false;
            this.Configuration.ProxyCreationEnabled = false;
        }

        //public ApplicationContext(string connectionStringName) :base(connectionStringName)
        //{

        //}

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<PluralizingEntitySetNameConvention>();

            SetModelconfigurations(modelBuilder);

        }


        public void SetModelconfigurations(DbModelBuilder modelBuilder)
        {
            
            modelBuilder.Configurations.Add(new DogConfig());
            modelBuilder.Configurations.Add(new AppointmentConfig());
            modelBuilder.Configurations.Add(new DogOwnerConfig());
            
        }

        public DbSet<IdentityUserRole> UserRoles { get; set; }

        public DbSet<IdentityUserClaim> UserClaims { get; set; }
         
        //public DbSet<Dog> Dogs { get; set; }

        public DbSet<DogOwner> DogOwners { get; set; }

        public DbSet<Breed> Breeds{ get; set; }

        public DbSet<Dog> Dogs { get; set; }
       

       public DbSet<Appointment> Appointments { get; set; }

        
        public static ApplicationContext Create()
        {
            return new ApplicationContext();
        }
    }
}
