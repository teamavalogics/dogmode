﻿using DogMode.Context;
using DogMode.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace DogMode.Admin.Controllers
{
    public class TestApiController : ApiController
    {

        private ApplicationContext Context; 


        public TestApiController()
        {
            Context = ApplicationContext.Create();
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            Context.Dispose();
        }



        // GET: api/TestApi/5
        public List<Appointment> Get()
        {
            var appointments = Context.Appointments.Take(10).ToList();

            var appointment = Context.Appointments.SingleOrDefault(a => a.Id == 333);

            Task.Run(async () =>
            {
                try
                {

                      Task.Delay(TimeSpan.FromSeconds(1));

                    var test = Context.Appointments.Any();

                    var breedsCount = Context.Breeds.Count();
                }
                catch(Exception ex)
                {

                }
                
            });


            return appointments;
        }

        // POST: api/TestApi
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/TestApi/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/TestApi/5
        public void Delete(int id)
        {
        }
    }
}
