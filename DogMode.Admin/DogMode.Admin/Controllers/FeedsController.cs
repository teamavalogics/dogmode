﻿using DogMode.BusinessAccess.Extensions;
using DogMode.BusinessAccess.Repositories;
using DogMode.Models;
using Newtonsoft.Json;
using PagedList;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace DogMode.Admin.Controllers
{
    //[Globalization]
    public class FeedsController : BaseController<FeedsRepository, Dog>
    {
        // GET: Feeds       
        [Authorize(Roles = "Admin,Publisher")]
        public ActionResult Index()
        {
           
            return View();
        }
        #region Grid admin  Feeds
   

        public async Task<ActionResult> Edit(int? id)
        {
            var model = id.HasValue ? (await Repository.FindByIdAsync(id.Value, "Campaing","Beacon")) : new Dog();

            
            return View(model);
        }

        
        [HttpPost]
        public async Task<ActionResult> Edit(Dog model)
        {
            if (ModelState.IsValid)
            {
                // Insert Or Update existing.
                Repository.InsertOrUpdate(model);
                

                await Repository.SaveAsync();

                return RedirectToAction("Index");
            }
            //ViewBag.Categories = (await Repository.GetCategoriesAsync()).ToSelectList(m => m.Name, m => m.Id.ToString(), "Select Category");

            return View(model);
        }

        public async Task<ActionResult> Details(int id)
        {
            var model = await Repository.FindByIdAsync(id, "Category", "User");


            return View("Partials/Details", model);
        }



        /// <summary>
        /// Ajax Async Search Grid.
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public ActionResult SearchAync(FilterOptionModel filter)
        {
            filter.companyId = ClaimsPrincipal.Current.CenterId();
            var model = Repository.GetSummary(filter);
            ViewBag.FilterModel = filter.Serialize();

            return View("Partials/ManageGrid", model.ToPagedList(filter.page == 0 ? 1 : filter.page, filter.Limit));
        }


        public ActionResult SearchFeeds(string filterdata, int? page, string columnName, string sortOrder)
        {
            var filter = JsonConvert.DeserializeObject<FilterOptionModel>(filterdata);
            var model = Repository.GetSummary(filter);

            filter.page = page != null ? (int)page : 1;

            ViewBag.FilterModel = JsonConvert.SerializeObject(filter);
            ViewBag.Filter = filter;

            if (!string.IsNullOrEmpty(columnName) && !string.IsNullOrEmpty(sortOrder))
            {
                ViewBag.CurrentColumnSort = columnName;
                ViewBag.SortOrder = sortOrder;
                return View("Partials/ManageGrid", model.OrderBy(columnName, sortOrder).ToPagedList(page ?? 1, filter.Limit));
            }

            return View("Partials/ManageGrid", model.ToPagedList(page ?? 1, filter.Limit));
        }

        [HttpPost]
        public ActionResult Sort(string filter, string columnName, string sortOrder, string currentColumn, int? page)
        {
            var _filter = JsonConvert.DeserializeObject<FilterOptionModel>(filter);
            var model = Repository.GetSummary(_filter);

            sortOrder = string.IsNullOrEmpty(sortOrder) ? "ASC" : sortOrder == "ASC" ? "DESC" : "ASC";

            if (currentColumn != null)
                sortOrder = columnName != currentColumn ? "ASC" : sortOrder;

            ViewBag.CurrentColumnSort = columnName;
            ViewBag.SortOrder = sortOrder;
            ViewBag.FilterModel = filter;

            return View("Partials/ManageGrid", model.OrderBy(columnName, sortOrder).ToPagedList(_filter.page, _filter.Limit));
        }




        public ActionResult Statuses()
        {
            var model = Enum.GetValues(typeof(DogStatus)).Cast<DogStatus>()
             .Select(i => new
             {
                 Id = (int)i,
                 Name = i.ToString(),
             }).ToList();

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> UpdateStatus(int id, DogStatus status)
        {
            // ViewBag.ActionMessage = Repository.MessageForStatus(status);
            var model = await Repository.FindByIdAsync(id, "User", "Category");
            model.Status = status;
            await Repository.SaveAsync();
            //return View("Partials/ConfirmAction", model);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]

        public async Task<ActionResult> sendpush(int id)
        {
            var model = await Repository.FindByIdAsync(id, "Category");
             
            //await NotificationsManager.SendNotificationAsync(model.Title, author, model.Id, model.CategoryId);

            return Json(true, JsonRequestBehavior.AllowGet);
        }
        

        public async Task<ActionResult> Delete(int id)
        {
            var model = await Repository.FindByIdAsync(id);

            return View(model);
        }
        [HttpPost]
        public async Task<ActionResult> Delete(int id, FormCollection collection)
        {

            await Repository.DeleteByIdAsync(id);

            return RedirectToAction("Index");
        }

        #endregion   Grid  admin feed  
    }
}