﻿using DogMode.BusinessAccess.Repositories;
using DogMode.Context;
using DogMode.Models;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
//using System.Web.Http.Cors;

namespace DogMode.Admin.Controllers
{
    //[EnableCors(origins: "*", headers: "*", methods: "*")]
    public class BaseApiController<TRepository, Tentity> : ApiController
         where TRepository : BaseRepository<ApplicationContext, Tentity>, new()
         where Tentity : BaseModel
    {

        protected ApplicationContext Context
        {
            get
            {
                return Request != null ?
                Request.GetOwinContext().Get<ApplicationContext>() :
                ApplicationContext.Create();
            }
        }



        private Type ModelType
        {
            get
            {
                return typeof(Tentity);
            }
        }

        protected TRepository Repository
        {
            get { return new TRepository() { Context = Context }; }
        }

        protected IHttpActionResult IhttpAction<Tmodel>(Tmodel model)
        {
            if (model == null) return NotFound();

            return Ok(model);
        }

        [HttpGet]
        [Route("Get")]
        public async Task<IHttpActionResult> Get(int id)
        {
            return IhttpAction(await Repository.FindByIdAsync(id));
        }


        //[HttpPost]
        //[Route("AddRange")]
        //public async Task<IHttpActionResult> Add(List<Tentity> models)
        //{
        //    return IhttpAction(await Repository.AddAsync(models));
        //}

        //[HttpPost]
        //[Route("Create")]
        //public async Task<IHttpActionResult> Add(Tentity model)
        //{
        //    return IhttpAction(await Repository.AddAsync(model));
        //}


        //[HttpPut]
        //[Route("Synchronize")]
        //public async Task<IHttpActionResult> Synchronize(List<Tentity> models)
        //{
        //    return IhttpAction(await Repository.UpdateAsync(models));
        //}


        //[HttpDelete]
        //[Route("Delete")]
        //public Task Delete(int id)
        //{
        //    return Repository.DeleteByIdAsync(id);
        //}



    }
}
