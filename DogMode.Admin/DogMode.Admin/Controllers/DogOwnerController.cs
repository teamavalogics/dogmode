﻿using DogMode.BusinessAccess.Extensions;
using DogMode.BusinessAccess.Repositories;
using DogMode.Context;
using DogMode.Models;
using Newtonsoft.Json;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace DogMode.Admin.Controllers
{

    [Authorize]
    public class DogOwnerController : BaseController<DogOwnerRepository,DogOwner>
    {
        // GET: DogOwner
        public ActionResult Index()
        {
            return View();
        }

        // GET: DogOwner/Details/5
        public async Task<ActionResult> Details(int id)
        {
            var model = await Repository.FindByIdAsync(id, "Dogs", "Dogs.DogBreed");

            ViewBag.Dogs = model.Dogs.ToList();

            return View(model);
        }



        // GET: DogOwner/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            
            return View(id.HasValue ? (await Repository.FindByIdAsync(id.Value)) : new DogOwner());
        }

        // POST: DogOwner/Edit/5
        [HttpPost]
        public async Task< ActionResult> Edit( DogOwner model)
        {
            if(ModelState.IsValid)
            {
                // TODO: Add update logic here
                Repository.InsertOrUpdate(model);

                await Repository.SaveAsync();

                return RedirectToAction("Index");
            }

            return View();
        }

        [HttpPost]
        /// <summary>
        /// Ajax Async Search Grid.
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public ActionResult SearchAsync(FilterOptionModel filter)
        {
             
            var model = Repository.GetSummary(filter);
            ViewBag.FilterModel = filter.Serialize();

            return View("Partials/ManageGrid", model.ToPagedList(filter.page == 0 ? 1 : filter.page, filter.Limit));
        }


        public ActionResult Search(string filterdata, int? page, string columnName, string sortOrder)
        {
            var filter = JsonConvert.DeserializeObject<FilterOptionModel>(filterdata);
            var model = Repository.GetSummary(filter);

            filter.page = page != null ? (int)page : 1;

            ViewBag.FilterModel = JsonConvert.SerializeObject(filter);
            ViewBag.Filter = filter;

            if (!string.IsNullOrEmpty(columnName) && !string.IsNullOrEmpty(sortOrder))
            {
                ViewBag.CurrentColumnSort = columnName;
                ViewBag.SortOrder = sortOrder;
                return View("Partials/ManageGrid", model.OrderBy(columnName, sortOrder).ToPagedList(page ?? 1, filter.Limit));
            }

            return View("Partials/ManageGrid", model.ToPagedList(page ?? 1, filter.Limit));
        }

        [HttpPost]
        public ActionResult Sort(string filter, string columnName, string sortOrder, string currentColumn, int? page)
        {
            var _filter = JsonConvert.DeserializeObject<FilterOptionModel>(filter);
            var model = Repository.GetSummary(_filter);

            sortOrder = string.IsNullOrEmpty(sortOrder) ? "ASC" : sortOrder == "ASC" ? "DESC" : "ASC";

            if (currentColumn != null)
                sortOrder = columnName != currentColumn ? "ASC" : sortOrder;

            ViewBag.CurrentColumnSort = columnName;
            ViewBag.SortOrder = sortOrder;
            ViewBag.FilterModel = filter;

            return View("Partials/ManageGrid", model.OrderBy(columnName, sortOrder).ToPagedList(_filter.page, _filter.Limit));
        }




        public ActionResult Statuses()
        {
            var model = Enum.GetValues(typeof(DogStatus)).Cast<DogStatus>()
             .Select(i => new
             {
                 Id = (int)i,
                 Name = i.ToString(),
             }).ToList();

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> UpdateStatus(int id, DogStatus status)
        {
            // ViewBag.ActionMessage = Repository.MessageForStatus(status);
            var model = await Repository.FindByIdAsync(id, "User", "Category");
           // model.Status = status;
            await Repository.SaveAsync();
            //return View("Partials/ConfirmAction", model);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]

        public async Task<ActionResult> sendpush(int id)
        {
            var model = await Repository.FindByIdAsync(id);

            //await NotificationsManager.SendNotificationAsync(model.Title, author, model.Id, model.CategoryId);

            return Json(true, JsonRequestBehavior.AllowGet);
        }


        public async Task<ActionResult> Delete(int id)
        {
            var model = await Repository.FindByIdAsync(id);

            return View(model);
        }
        [HttpPost]
        public async Task<ActionResult> Delete(int id, FormCollection collection)
        {
            DogOwner model = null;
            using (var context = ApplicationContext.Create())
            {
                var repo = new DogOwnerRepository() { Context = context };
                model = await repo.FindByIdAsync(id);
            }

            try
            {

            await Repository.DeleteByIdAsync(id);

            return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                // TODO log this
                ViewBag.Error = "Cannot delete; must delete associated dog(s) first.";
                return View(model);
            }
        }
    }
}
