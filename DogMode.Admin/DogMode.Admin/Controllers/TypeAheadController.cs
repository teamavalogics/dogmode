﻿using DogMode.BusinessAccess.Repositories;
using DogMode.Context;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace DogMode.Admin.Controllers
{
    public class TypeAheadController : Controller
    {

        protected ApplicationContext Context
        {
            get
            {

                return Request.GetOwinContext().Get<ApplicationContext>();
            }
        }

        public async Task<ActionResult> GetBreeds(string query)
        {
            var repository = new BreedsRepository() { Context = Context };

            return Json(await repository.SearchAsync(query), JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> GetOwner(string query)
        {
            var repository = new DogOwnerRepository() { Context = Context };

            return Json(await repository.SearchForAutoCompleteAsync(query), JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> GetDogsForAutoComplete(string query)
        {
            var repository = new DogRepository() { Context = Context };

            return Json(await repository.SearchForAutoComplete(query), JsonRequestBehavior.AllowGet);
        }

        
        //public async Task <ActionResult> GetCompaniesForAutoComplete(string query)
        //{
        //    var repository = new CompaniesRepository() { Context = Context };

        //    var companies = await repository.GetCompanyNamesAsync(query);

        //    return Json(companies,JsonRequestBehavior.AllowGet);
        //}



    }
}