﻿using DogMode.BusinessAccess.Repositories;
using DogMode.Context;
using DogMode.Models;
using Microsoft.AspNet.Identity.Owin;
using System.Web;
using System.Web.Mvc;

namespace DogMode.Admin.Controllers
{
    //[RequireHttps]
    public class BaseController<TRepository, TEntity> : Controller //new()
                                                                   //where TContext : DbContext
        where TEntity : BaseModel
        where TRepository : BaseRepository<ApplicationContext, TEntity>, new()
    {




        protected ApplicationContext Context
        {
            get
            {

                return Request.GetOwinContext().Get<ApplicationContext>();
            }
        }

        protected TRepository Repository
        {
            get
            {
                return new TRepository()
                {
                    Context = Context
                };
            }
        }


        //public Guid? LoggedUser
        //{
        //    get
        //    {
        //        if (!User.Identity.IsAuthenticated) return null;

        //        var id = User.Identity.GetUserId();

        //        return new Guid(id);
        //    }
        //}
    }
}