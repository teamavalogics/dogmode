﻿using DogMode.BusinessAccess.Repositories;
using DogMode.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace DogMode.Admin.Controllers
{

    [RoutePrefix("api/appointments")]
    public class AppointmentApiController : BaseApiController<AppointmentsRepository,Appointment>
    {

        public AppointmentApiController()
        {

        }
        [Route("Keepalive")]
        [HttpGet]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> KeepAlive()
        {
            return IhttpAction(await Repository.HasAppointmentsAsync());
        }

        [Route("get")]
        [HttpGet]
        [ResponseType(typeof(Appointment))]
        public async Task<IHttpActionResult> get(int id)
        {
            var model = await Repository.FindByIdAsync(id);
            return IhttpAction(model);
        }

        /// <summary>
        /// Search Feeds By filter Options 
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Search")]
        [ResponseType(typeof(List<Appointment>))]
        public async Task<IHttpActionResult> Search(FilterOptionModel filter)
        {
            var models = await Repository.SearchAsync(filter);

            return IhttpAction(models);
        }

        [HttpGet]
        [Route("Search")]
        [ResponseType(typeof(List<Appointment>))]
        public async Task<IHttpActionResult> Search()
        {
            var appointments = await Repository.SearchAsync();

            return IhttpAction(appointments);
        }


        [HttpGet]
        [Route("Search")]
        [ResponseType(typeof(List<Appointment>))]
        public async Task<IHttpActionResult> Search(DateTime date)
        {
            var appointments = await Repository.SearchAsync(date);

            return IhttpAction(appointments);
        }


        [HttpGet]
        [Route("Search")]
        [ResponseType(typeof(List<Appointment>))]
        public async Task<IHttpActionResult> Search(AppointmentType type)
        {
            var appointments = await Repository.SearchAsync(type);

            return IhttpAction(appointments);
        }

     
    }
}
