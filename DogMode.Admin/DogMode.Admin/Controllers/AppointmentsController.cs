﻿using DogMode.Admin.Hub;
using DogMode.BusinessAccess.Extensions;
using DogMode.BusinessAccess.Repositories;
using DogMode.Context;
using DogMode.Models;
using Microsoft.AspNet.SignalR;
using Newtonsoft.Json;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace DogMode.Admin.Controllers
{

    [Globalization]
    public class AppointmentsController : BaseController<AppointmentsRepository, Appointment>
    {
        // GET: DogOwner
        public ActionResult Index()
        {
            return View();
        }

        // GET: DogOwner/Details/5
        public async Task<ActionResult> Details(int id)
        {
            return View(await Repository.GetDetailedAppointmentAsync(id));
        }



        // GET: DogOwner/Edit/5
        public async Task<ActionResult> Add(int? id)
        {
            
           return id.HasValue? View(await Repository.GetAppointmentViewModelFromDogId(id.Value)):  View();
        }

        public async Task<ActionResult> EditAppointment(int id)
        {
            return View(await Repository.FindByIdAsync(id));
        }

        public async Task<ActionResult> Edit(int id)
        {
            return View(await Repository.FindByIdAsync(id,"Dog","Dog.DogBreed"));
        }

        // POST: DogOwner/Edit/5
        [HttpPost]
        public async Task<ActionResult> Edit(Appointment model)
        {
            if (ModelState.IsValid)
            {
                decimal paidHours = await Repository.GetPaidHoursAsync(model.Id) ;

                if (model.PaidHours  != paidHours) model.CheckOut = model.CheckIn.AddHours((double) model.PaidHours);

                // TODO: Add update logic here
                Repository.InsertOrUpdate(model);

                await Repository.SaveAsync();

                FireSyncAction(null);

                return RedirectToAction("Index");
            }

            model.Dog = await Repository.FindDogAsync(model.DogId);

            return View(model);
        }

         
        [HttpGet]
        public async Task<JsonResult> GetDogsForOwner(int ownerId)
        {
            return Json(await Repository.GetDogsAppointmentViewModelAsync(ownerId), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<JsonResult> CreateAppointments(List<DogAppointmentViewModel> appointments)
        {

            var result = await Repository.CreateAppointments(appointments);

            if (result.success) FireSyncAction(null);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<JsonResult> UpdateStatus(int id,QueueStatus status )
        {
            var result = await Repository.UpdateStatusAsync(id, status);

            var syncModel = await Repository.FindAppointmentViewModel(id);

            FireSyncAction(syncModel);

            return Json(result, JsonRequestBehavior.AllowGet);
        }


        [HttpGet] 
        public ActionResult CreateNewDog()
        {
            return View("Partials/CreateNewDog", new Dog());
        }

        [HttpPost]
        public async Task< JsonResult >CreateNewDog(Dog model)
        {
            if (ModelState.IsValid || (model.DogOwnerId!=0 && model.DogBreedId!=0))
            {
                var result = await Repository.CreateNewDogAsync(model);

                var ownerName =await Repository.GetOwnerNameAsync(model.DogOwnerId);

                var json = new
                {
                    OwnerId = model.DogOwnerId,
                    DogId = model.Id,
                    status = result.success ? "ok" : "error",
                    OwnerName = ownerName,
                    message = result.message
                };

                return Json(json,JsonRequestBehavior.AllowGet);
            }
            return Json(new { OwnerId = model.DogOwnerId, DogId = model.Id, status = "error" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        /// <summary>
        /// Ajax Async Search Grid.
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public ActionResult SearchAsync(FilterOptionModel filter)
        {

            var model = Repository.GetSummary(filter);
            ViewBag.FilterModel = filter.Serialize();

            return View("Partials/ManageGrid", model.ToPagedList(filter.page == 0 ? 1 : filter.page, filter.Limit));
        }


        public ActionResult Search(string filterdata, int? page, string columnName, string sortOrder)
        {
            var filter = JsonConvert.DeserializeObject<FilterOptionModel>(filterdata);
            var model = Repository.GetSummary(filter);

            filter.page = page != null ? (int)page : 1;

            ViewBag.FilterModel = JsonConvert.SerializeObject(filter);
            ViewBag.Filter = filter;

            if (!string.IsNullOrEmpty(columnName) && !string.IsNullOrEmpty(sortOrder))
            {
                ViewBag.CurrentColumnSort = columnName;
                ViewBag.SortOrder = sortOrder;
                return View("Partials/ManageGrid", model.OrderBy(columnName, sortOrder).ToPagedList(page ?? 1, filter.Limit));
            }

            return View("Partials/ManageGrid", model.ToPagedList(page ?? 1, filter.Limit));
        }

        [HttpPost]
        public ActionResult Sort(string filter, string columnName, string sortOrder, string currentColumn, int? page)
        {
            var _filter = JsonConvert.DeserializeObject<FilterOptionModel>(filter);
            var model = Repository.GetSummary(_filter);

            sortOrder = string.IsNullOrEmpty(sortOrder) ? "ASC" : sortOrder == "ASC" ? "DESC" : "ASC";

            if (currentColumn != null)
                sortOrder = columnName != currentColumn ? "ASC" : sortOrder;

            ViewBag.CurrentColumnSort = columnName;
            ViewBag.SortOrder = sortOrder;
            ViewBag.FilterModel = filter;

            return View("Partials/ManageGrid", model.OrderBy(columnName, sortOrder).ToPagedList(_filter.page, _filter.Limit));
        }




        public ActionResult Gettypes()
        {
            return Json(CollectionExtensions.AsEnumList<AppointmentType>(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetTypesByDescending()
        {
            return Json(CollectionExtensions.AsEnumList<AppointmentType>().OrderByDescending(t => t.Id).ToList(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetStatuses()
        {
            return Json(CollectionExtensions.AsEnumList<QueueStatus>(), JsonRequestBehavior.AllowGet);
        }

         

        public async Task<ActionResult> UpdateStatus(int id, DogStatus status)
        {
            // ViewBag.ActionMessage = Repository.MessageForStatus(status);
            var model = await Repository.FindByIdAsync(id, "User", "Category");
            // model.Status = status;
            await Repository.SaveAsync();
            //return View("Partials/ConfirmAction", model);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]

        public async Task<ActionResult> sendpush(int id)
        {
            var model = await Repository.FindByIdAsync(id);

            //await NotificationsManager.SendNotificationAsync(model.Title, author, model.Id, model.CategoryId);

            return Json(true, JsonRequestBehavior.AllowGet);
        }


        public async Task<ActionResult> Delete(int id)
        {
            var model = await Repository.GetDetailedAppointmentAsync(id);

            return View(model);
        }
        [HttpPost]
        public async Task<ActionResult> Delete(int id, FormCollection collection)
        {

            await Repository.DeleteByIdAsync(id);

            FireSyncAction(null);

            return RedirectToAction("Index");
        }

        private void FireSyncAction(AppointmentViewModel model)
        {
             HubContext.Clients.All.Sync(model);
        }
        //context.Clients.All.FireSync(model);

        private IHubContext HubContext
        {
            get
            {
                return GlobalHost.ConnectionManager.GetHubContext<NotifyHub>();
                
            }
        }
    }
}