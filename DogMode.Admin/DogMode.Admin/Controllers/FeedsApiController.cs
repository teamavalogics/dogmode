﻿using Extensions;
using DogMode.BusinessAccess.Repositories;
using DogMode.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace Inbound.MobileService.Controllers
{
    [RoutePrefix("api/feeds")]
    public class FeedsApiController : BaseApiController<FeedsRepository, Dog>
    {

        [Route("Keepalive")]
        [HttpGet]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> KeepAlive()
        {
            return IhttpAction(await Repository.HasActiveDogs());
        }

        /// <summary>
        /// Find By Id 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("Find")]
        [HttpGet]
        [ResponseType(typeof(Dog))]
        public async Task<IHttpActionResult> Find(int id)
        {
            var model = await Repository.FindByIdAsync(id);
            return IhttpAction(model);
        }

       
        
        /// <summary>
        ///  Add new Feed 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Add")]
        public async Task<IHttpActionResult> Add(Dog model)
        {
            return IhttpAction(await Repository.AddAsync(model));
        }

        /// <summary>
        /// Search Feeds By filter Options 
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Search")]
        [ResponseType(typeof(List<Dog>))]
        public async Task<IHttpActionResult> GetFeeds(FilterOptionModel filter)
        {
            var models = await Repository.GetSummaryAsync(filter);

            return IhttpAction(models);
        }

     


        [HttpPost]
        [Route("Upload")]
        [ResponseType(typeof(List<string>))]
        public IHttpActionResult Upload(IEnumerable<HttpPostedFileBase> files)
        {
            var fileNames = files.Select(async f => await AzureHelper.UploadAsync(f.InputStream, f.FileName)).ToList();

            return IhttpAction(fileNames);
        }


        [HttpPost]
        [Route("PostFile")]
        [ResponseType(typeof(List<string>))]
        public async Task<IHttpActionResult> PostFile()
        {
            // Check if the request contains multipart/form-data.
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            var streamProvider = new MultipartMemoryStreamProvider();


            await Request.Content.ReadAsMultipartAsync<MultipartMemoryStreamProvider>(streamProvider);

            var fileNames = new List<string>();

            foreach (HttpContent ctnt in streamProvider.Contents)
            {
                // You would get hold of the inner memory stream here
                Stream stream = await ctnt.ReadAsStreamAsync();

                var name = await AzureHelper.UploadAsync(stream, "image.jpg");

                fileNames.Add(name);



                // do something witht his stream now
            }

            return IhttpAction(fileNames);

        }


        //image = RotateImage(image, 90);
        //Stream memoryStream = new MemoryStream();
        //image.Save(memoryStream, System.Drawing.Imaging.ImageFormat.Jpeg);


        //public Bitmap RotateImage(Bitmap bitmap, float angle)
        //{
        //    Bitmap returnBitmap = new Bitmap(bitmap.Width, bitmap.Height);
        //    Graphics graphics = Graphics.FromImage(returnBitmap);
        //    graphics.TranslateTransform((float)bitmap.Width / 2, (float)bitmap.Height / 2);
        //    graphics.RotateTransform(angle);
        //    graphics.TranslateTransform(-(float)bitmap.Width / 2, -(float)bitmap.Height / 2);
        //    graphics.DrawImage(bitmap, new Point(0, 0));
        //    return returnBitmap;
        //}

    }
}
