﻿using DogMode.BusinessAccess.Extensions;
using DogMode.BusinessAccess.Repositories;
using DogMode.Models;
using Newtonsoft.Json;
using PagedList;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace DogMode.Admin.Controllers
{
    public class StatisticsController : BaseController<StatisticsRepository, Dog>
    {
        // GET: Statistics
        public async Task<ActionResult> Index()
        { 
            return View();
        }

        #region Grid Functions

        /// <summary>
        /// Ajax Async Search Grid.
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SearchAync(FilterOptionModel filter)
        {
            filter.companyId = ClaimsPrincipal.Current.CenterId();
            var model = Repository.Search(filter);
            ViewBag.FilterModel = filter.Serialize();


            return View("Partials/StatisticsGrid", model.ToPagedList(filter.page == 0 ? 1 : filter.page, filter.Limit));
        }

        public ActionResult SearchFeeds(string filterdata, int? page, string columnName, string sortOrder)
        {
            var filter = JsonConvert.DeserializeObject<FilterOptionModel>(filterdata);
            var model = Repository.Search(filter);

            filter.page = page != null ? (int)page : 1;

            ViewBag.FilterModel = JsonConvert.SerializeObject(filter);
            ViewBag.Filter = filter;

            if (!string.IsNullOrEmpty(columnName) && !string.IsNullOrEmpty(sortOrder))
            {
                ViewBag.CurrentColumnSort = columnName;
                ViewBag.SortOrder = sortOrder;
                return View("Partials/StatisticsGrid", model.OrderBy(columnName, sortOrder).ToPagedList(page ?? 1, filter.Limit));
            }

            return View("Partials/StatisticsGrid", model.ToPagedList(page ?? 1, filter.Limit));
        }

        [HttpPost]
        public ActionResult Sort(string filter, string columnName, string sortOrder, string currentColumn, int? page)
        {
            var _filter = JsonConvert.DeserializeObject<FilterOptionModel>(filter);
            var model = Repository.Search(_filter);

            sortOrder = string.IsNullOrEmpty(sortOrder) ? "ASC" : sortOrder == "ASC" ? "DESC" : "ASC";

            if (currentColumn != null)
                sortOrder = columnName != currentColumn ? "ASC" : sortOrder;

            ViewBag.CurrentColumnSort = columnName;
            ViewBag.SortOrder = sortOrder;
            ViewBag.FilterModel = filter;

            return View("Partials/StatisticsGrid", model.OrderBy(columnName, sortOrder).ToPagedList(_filter.page == 0 ? 1 : _filter.page, _filter.Limit));
        }

        #endregion Grid Regions


        #region Summary

        public async Task<ActionResult> MostViewed()
        {
            var model = await Repository.GetMostViewedFeeds();

            return View("Partials/MostViewed", model);

        }

        public async Task<ActionResult> ViewsPerMonths()
        {
            return Json(await Repository.ViewsLastSixMonthsAsync(), JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> ViewsLastMonth()
        {
            return Json(await Repository.ViewsLasMonthAsync(), JsonRequestBehavior.AllowGet);
        }
        #endregion     Summary
    }
}