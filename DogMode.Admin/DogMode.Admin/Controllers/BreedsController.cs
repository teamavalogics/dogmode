﻿using DogMode.BusinessAccess.Repositories;
using DogMode.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace DogMode.Admin.Controllers
{
    public class BreedsController : BaseController<BreedsRepository, Breed>
    {
        // GET: Breeds
        public async Task<ActionResult> Index()
        {
            return View(await Repository.GetAllAsync());
        }

        // GET: Breeds/Details/5
        public async Task<ActionResult> Details(int id)
        {
            return View(await Repository.FindByIdAsync(id));
        }


        // GET: Breeds/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            return View(id.HasValue ? await Repository.FindByIdAsync(id.Value) : new Breed());
        }

        // POST: Breeds/Edit/5
        [HttpPost]
        public async Task< ActionResult> Edit(Breed model)
        {
            if (ModelState.IsValid)
            {

                Repository.InsertOrUpdate(model);

                await Repository.SaveAsync();

                return RedirectToAction("Index");
            }
                return View();
            
        }

        // GET: Breeds/Delete/5
        public async Task< ActionResult>Delete(int id)
        {
            return View(await Repository.FindByIdAsync(id) );
        }

        // POST: Breeds/Delete/5
        [HttpPost]
        public async Task<ActionResult> DeleteModel(int id)
        {
            try
            {
                var model = await Repository.FindByIdAsync(id);
                await Repository.DeleteAsync(model);
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
