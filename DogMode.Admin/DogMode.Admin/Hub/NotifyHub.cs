﻿using DogMode.Models;
using Microsoft.AspNet.SignalR.Hubs;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace DogMode.Admin.Hub
{
    [HubName("NotifyHub")]
    public class NotifyHub : Microsoft.AspNet.SignalR.Hub
    {
        //public void Send(string name, string message)
        //{
        //    // Call the addNewMessageToPage method to update clients.
        //    Clients.All.addNewMessageToPage(name, message);

        //}



        public void Send(string key, string message)
        {
            foreach (var connectionId in _connections.GetConnections(key))
            {

                Clients.Client(connectionId).SendMessage(message);
            }
        }



        public void GetDevices(string key)
        {
            var connections = _connections.GetConnectionIds()
                .Select(id => new { Key = id, Name = "Device" }).ToList();

            foreach (var connectionId in _connections.GetConnections(key))
            {

                Clients.Client(connectionId).GetDevicesCallBack(connections);
            }
        }

        public void OnDeviceAdded(string key)
        {
            //get connection for admin
            foreach (var connectionId in _connections.GetConnections("admin"))
            {
                //device name added just for test purpose
                Clients.Client(connectionId).OnDeviceAddedCallBack(new { Key = key, Name = "Device" });
            }
        }


        public void GetAvailableActions(string key)
        {
            foreach (var connectionId in _connections.GetConnections(key))
            {

                Clients.Client(connectionId).GetAvailableActions();
            }
        }

      

 
        public async void Sync(AppointmentViewModel model)
        {
            var connections = _connections.GetConnectionIds()
                .SelectMany(c=> _connections.GetConnections(c));

            foreach (var connectionId in connections)
            {

                if (string.IsNullOrEmpty(connectionId))
                {
                    AddToPendingNotifications(connectionId, model);
                    continue;
                }

                var date = DateTime.Now.ToString(CultureInfo.InstalledUICulture);

                await Clients.Client(connectionId).Sync(model);
            }
        }

        public override Task OnConnected()
        {

            var key = Context.QueryString["key"];

            _connections.Add(key, Context.ConnectionId);

           // OnDeviceAdded(key);
            return base.OnConnected();

        }


        public override Task OnReconnected()
        {
            var whscode = Context.QueryString["Key"] ?? string.Empty;
            if (!_connections.GetConnections(whscode).Contains(Context.ConnectionId))
            {
                _connections.Add(whscode, Context.ConnectionId);
            }

            SendPendingNotifications(whscode);
            return base.OnReconnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            return base.OnDisconnected(stopCalled);
        }
        private void AddToPendingNotifications(string key, object notifications = null)
        {
            object notify;
            if (!pendingNotifications.ContainsKey(key))
            {
                pendingNotifications.Add(key, notifications);
            }
        }

        private void RemoveFormPendingNotifications(string key)
        {
            if (pendingNotifications.ContainsKey(key))
            {
                pendingNotifications.Remove(key);
            }
        }

        private void SendPendingNotifications(string key)
        {
            if (string.IsNullOrEmpty(key)) return;

            if (pendingNotifications.ContainsKey(key)) Sync(pendingNotifications[key]);

            RemoveFormPendingNotifications(key);

        }




        private static ConnectionMapping<string> _connections = new ConnectionMapping<string>();
        private static Dictionary<string, dynamic> pendingNotifications = new Dictionary<string, dynamic>();

    }
}