﻿using DogMode.Admin.Models;
using DogMode.BusinessAccess.Extensions;
using DogMode.BusinessAccess.Services;
using DogMode.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DogMode.Admin
{
    public static class IdentityHelpers
    {

        public static IdentityUserViewModel ToIdentityUserViewModel(this ApplicationUser model)
        {
            var i = new IdentityUserViewModel();
            i.Assign(model);

            return i;
        }
        public static async Task<IdentityUserViewModel> ToIdentityUserViewModelAsync(this ApplicationUser model, IdentityManagerService service)
        {
            var i = new IdentityUserViewModel();
            i.Assign(model);

            //if (model.Company != null)
            //{
            //    i.CompanyId = model.CompanyId.HasValue ? model.CompanyId.Value : 0;
            //    i.CompanyNameForAutoComplete = model.Company.Name;
            //}

            if (service != null) i.Role = await service.GetMainRoleForUserAsync(model.Id) ?? "";

            return i;
        }


        public static List<IdentityUserViewModel> ToIdentityUserViewModel(this List<ApplicationUser> model)
        {
            return model.Select(m => m.ToIdentityUserViewModel()).ToList();
        }




        
    }
}