﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Http.Filters;
using System.Web.Mvc;

namespace DogMode.Admin
{
    public sealed class Globalization : System.Web.Mvc.ActionFilterAttribute
    {

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            GlobalizationStart Globalization = new GlobalizationStart();
        }

        private class GlobalizationStart
        {
            public GlobalizationStart()
            {
                // Hardcoded
                string culture = "en-US";


                var cultureInfo = new CultureInfo(culture ?? FromBrowser());
                Thread.CurrentThread.CurrentCulture = cultureInfo;
                Thread.CurrentThread.CurrentUICulture = cultureInfo;

            }
            private string FromBrowser()
            {
                if (HttpContext.Current == null || HttpContext.Current.Request == null ||
                    HttpContext.Current.Request.UserLanguages == null)
                    return "en";

                var lang = HttpContext.Current.Request.UserLanguages.FirstOrDefault();

                return !string.IsNullOrEmpty(lang) ? lang : "en-US";
            }


        }



    }
}